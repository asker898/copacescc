﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Windows.Data.Xml.Dom;
using Windows.UI.Xaml;

namespace GDOTPACESLib
{
    public class ConfigFileReader
    {
        Assembly asm = typeof(ConfigFileReader).GetTypeInfo().Assembly;
        readonly Func<XElement, string, int> parseIntAttr = (XElement x, string attr) => Int32.Parse(x.Attribute(attr).Value);

        public int QueryXmlForDeduct(int severity, int distressValue, string xmlFile)
        {
            if (severity == 0)
                return 0;

            var root = XDocument.Load(asm.GetManifestResourceStream(xmlFile));
            var res = root.Descendants("Severity")
                .First(x => parseIntAttr(x, "value") == severity)
                .Descendants("Deduct")
                .First(e => parseIntAttr(e, "MinDistress") <= distressValue && parseIntAttr(e, "MaxDistress") >= distressValue).Value;
            return Convert.ToInt32(res);
        }

        public int QueryXmlForNoSeverityDeduct(int distressValue, string xmlFile)
        {
            var root = XDocument.Load(asm.GetManifestResourceStream(xmlFile));
            var res = root.Descendants("Deduct")
                .First(e => parseIntAttr(e, "MinDistress") <= distressValue && parseIntAttr(e, "MaxDistress") >= distressValue).Value;
            return Convert.ToInt32(res);
        }

        /// <summary>
        /// Looks for supplied severity and distress in the Bleed look up table.
        /// </summary>
        public int GetBleedingDistress(int severity, int distressValue)
        {
            return QueryXmlForDeduct(severity, distressValue, "GDOTPACESLib.Bleed_Lov.xml");
        }

        /// <summary>
        /// Looks for supplied severity and distress in the block cracking look up table. 
        /// </summary>
        public int GetBlockCrackingDistress(int severity, int distressValue)
        {
            if (severity == 0)
                return 0;

            var root = XDocument.Load(asm.GetManifestResourceStream("GDOTPACESLib.Block_Crack_Lov.xml"));
            var res = root.Descendants("Severity")
                .First(x => parseIntAttr(x, "value") == severity)
                .Descendants("Deduct")
                .First(e => parseIntAttr(e, "DistressValue") == distressValue).Value;
            return Convert.ToInt32(res);
        }

        /// <summary>
        /// Looks for supplied severity and distress in the block corrugation look up table. 
        /// </summary>
        public int GetCorrugationDistress(int severity, int distressValue)
        {
            return  QueryXmlForDeduct(severity, distressValue, "GDOTPACESLib.Corrugation_Lov.xml");
        }
        /// <summary>
        /// Looks for supplied severity and distress in the edge distress look up table
        /// </summary>
        public int GetEdgeDistress(int severity, int distressValue)
        {
            return  QueryXmlForDeduct(severity, distressValue, "GDOTPACESLib.Edge_Lov.xml");
        }
        /// <summary>
        /// Looks for supplied severity and distress in the load cracking look up table
        /// </summary>
        public int GetLoadCrackingDistress(int severity, int distressValue)
        {
            if (severity == 0)
                return 0;

            var root = XDocument.Load(asm.GetManifestResourceStream("GDOTPACESLib.Load_Crack_Lov.xml"));
            var res = root.Descendants("Severity")
                .First(x => parseIntAttr(x, "value") == severity)
                .Descendants("Deduct")
                .First(e => parseIntAttr(e, "DistressValue") == distressValue).Value;
            return Convert.ToInt32(res);
        }
        /// <summary>
        /// Looks for supplied severity and distress in the loss of pavement section look up table
        /// </summary>
        public int GetLossSectionDistress(int severity, int distressValue)
        {
            return  QueryXmlForDeduct(severity, distressValue, "GDOTPACESLib.Loss_Sect_Lov.xml");
        }
        /// <summary>
        /// Looks for supplied severity and distress in the patches and potholes look up table
        /// </summary>
        public int GetPatchAndPotholesDistress(int distressValue)
        {
            return QueryXmlForNoSeverityDeduct(distressValue, "GDOTPACESLib.Patch_Lov.xml");
        }

        /// <summary>
        /// Looks for supplied severity and distress in the raveling distress look up table.
        /// </summary>
        public int GetRavelingDistress(int severity, int distressValue)
        {
            return  QueryXmlForDeduct(severity, distressValue, "GDOTPACESLib.Ravel_Lov.xml");
        }

        /// <summary>
        /// Looks for supplied severity and distress in the reflection cracking distress look up table.
        /// </summary>
        public int GetReflectionCrackingDistress(int severity, int distressValue)
        {
            return  QueryXmlForDeduct(severity, distressValue, "GDOTPACESLib.Reflect_Lov.xml");
        }

        /// <summary>
        /// Looks for the rut depth distress in the rut depth look up table.
        /// </summary>
        public int GetRutDepthDistress(int distressValue)
        {
            if (distressValue > 8) return 24;

            var root = XDocument.Load(asm.GetManifestResourceStream("GDOTPACESLib.Rut_Depth_Lov.xml"));
            var res = root
                .Descendants("Deduct")
                .First(e => parseIntAttr(e, "DistressValue") == distressValue).Value;
            return Convert.ToInt32(res);
        }
    }
}
