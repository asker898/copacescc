﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace GDOTPACESLib
{
    public class Project
    {
        #region Private variables and their property methods

        Collection<Segment> _segmentsCollection;
        int _pacesRating = 0;
        int _outsideWPSum = 0;        
        int _insideWPSum = 0;
        int _blockCrackingPercentageSum = 0;
        int _blockCrackingSeveritySum = 0;
        int _blockCrackingSeverityCount = 0;
        int _loadCrackingSeverityLevel1PercentageSum = 0;
        int _loadCrackingSeverityLevel2PercentageSum = 0;
        int _loadCrackingSeverityLevel3PercentageSum = 0;
        int _loadCrackingSeverityLevel4PercentageSum = 0;
        int _bleedingPercentageSum = 0;
        int _bleedingSeveritySum = 0;
        int _bleedingSeverityCount = 0;
        int _ravelingPercentageSum = 0;
        int _ravelingSeveritySum = 0;
        int _ravelingSeverityCount = 0;
        int _edgeDistressPercentageSum = 0;
        int _edgeDistressSeveritySum = 0;
        int _edgeDistressSeverityCount = 0;
        int _lossOfPavementPercentageSum = 0;
        int _lossOfPavementSeveritySum = 0;
        int _corrugationPercentageSum = 0;
        int _corrugationSeveritySum = 0;
        int _corrugationSeverityCount = 0;
        int _reflectionCrackTotalLengthSum = 0;
        int _reflectionCrackSeveritySum = 0;
        int _reflectionCrackSeverityCount = 0;
        int _patchesAndPotholesSum = 0;
        int _numberOfSegments = 0;
        int _reflectionCrackNumberSum = 0;

        int _rutDepthDeduct = 0;
        int _loadCrackingDeduct = 0;
        int _blockCrackingDeduct = 0;
        int _ravelingDeduct = 0;
        int _edgeDistressDeduct = 0;
        int _bleedingDeduct = 0;
        int _lossOfPavementDeduct = 0;
        int _corrugationDeduct = 0;
        int _reflectionDeduct = 0;
        int _patchesAndPotholesDeduct = 0;
        int _crossSlopeDeduct = 0;
        int _loadCrackingSeverity1Deduct = 0;
        int _loadCrackingSeverity2Deduct = 0;
        int _loadCrackingSeverity3Deduct = 0;
        int _loadCrackingSeverity4Deduct = 0;

        int _rutDepthMax = 0;
        int _loadCrackingPercentageSeverity1Average = 0;
        int _loadCrackingPercentageSeverity2Average = 0;
        int _loadCrackingPercentageSeverity3Average = 0;
        int _loadCrackingPercentageSeverity4Average = 0;
        int _blockCrackingPercentageAverage = 0;
        int _blockCrackingSeverityAverage = 0;
        int _reflectionLengthAverage = 0;
        int _reflectionSeverityAverage = 0;
        int _ravelingPercentageAverage = 0;
        int _ravelingSeverityAverage = 0;
        int _edgeDistressPercentageAverage = 0;
        int _edgeDistressSeverityAverage = 0;
        int _bleedingPercentageAverage = 0;
        int _bleedingSeverityAverage = 0;
        int _corrugationPercentageAverage = 0;
        int _corrugationSeverityAverage = 0;
        int _lossOfPavementPercentageAverage = 0;
        int _lossOfPavementSeverityAverage = 0;
        int _crossSlopeAverage = 0;
        int _patchesAndPotholesAverage = 0;
        int _crossSlopeMax = 0;
        int _reflectionNumberAverage = 0;

        /// <summary>
        /// Holds the average of the number of reflection cracks
        /// </summary>
        public int ReflectionNumberAverage
        {
            get { return _reflectionNumberAverage; }
            private set { _reflectionNumberAverage = value; }
        }

        /// <summary>
        /// Holds the maximum of Cross slope values.
        /// </summary>
        public int CrossSlopeMax
        {
            get { return _crossSlopeMax; }
            private set { _crossSlopeMax = value; }
        }
        /// <summary>
        ///Holds average of Load cracking severity level 1 percentage
        /// </summary>
        public int LoadCrackingPercentageSeverity1Average
        {
            get { return _loadCrackingPercentageSeverity1Average; }
            private set { _loadCrackingPercentageSeverity1Average = value; }
        }
       
        /// <summary>
        /// Holds average of Load cracking severity level 2 percentage
        /// </summary>
        public int LoadCrackingPercentageSeverity2Average
        {
            get { return _loadCrackingPercentageSeverity2Average; }
            private set { _loadCrackingPercentageSeverity2Average = value; }
        }
        
        /// <summary>
        /// Holds average of Load cracking severity level 3 percentage
        /// </summary>
        public int LoadCrackingPercentageSeverity3Average
        {
            get { return _loadCrackingPercentageSeverity3Average; }
            private set { _loadCrackingPercentageSeverity3Average = value; }
        }
        
        /// <summary>
        /// Holds average of Load cracking severity level 4 percentage
        /// </summary>
        public int LoadCrackingPercentageSeverity4Average
        {
            get { return _loadCrackingPercentageSeverity4Average; }
            private set { _loadCrackingPercentageSeverity4Average = value; }
        }
        
        /// <summary>
        /// Holds average of Block Cracking percentage
        /// </summary>
        public int BlockCrackingPercentageAverage
        {
            get { return _blockCrackingPercentageAverage; }
            private set { _blockCrackingPercentageAverage = value; }
        }
        
        /// <summary>
        /// Holds average of Block Cracking severity
        /// </summary>
        public int BlockCrackingSeverityAverage
        {
            get { return _blockCrackingSeverityAverage; }
            private set { _blockCrackingSeverityAverage = value; }
        }
        
        /// <summary>
        /// Holds average of Reflection Cracking percentage
        /// </summary>
        public int ReflectionLengthAverage
        {
            get { return _reflectionLengthAverage; }
            private set { _reflectionLengthAverage = value; }
        }
       
        /// <summary>
        /// Holds average of Reflection Cracking Severity
        /// </summary>
        public int ReflectionSeverityAverage
        {
            get { return _reflectionSeverityAverage; }
            private set { _reflectionSeverityAverage = value; }
        }
        
        /// <summary>
        /// Holds average of Raveling percentage
        /// </summary>
        public int RavelingPercentageAverage
        {
            get { return _ravelingPercentageAverage; }
            private set { _ravelingPercentageAverage = value; }
        }
       
        /// <summary>
        /// Holds average of Raveling severity
        /// </summary>
        public int RavelingSeverityAverage
        {
            get { return _ravelingSeverityAverage; }
            private set { _ravelingSeverityAverage = value; }
        }
        
        /// <summary>
        /// Holds average of Edge Distress percentage
        /// </summary>
        public int EdgeDistressPercentageAverage
        {
            get { return _edgeDistressPercentageAverage; }
            private set { _edgeDistressPercentageAverage = value; }
        }
       
        /// <summary>
        /// Holds average of Edge Distress severity
        /// </summary>
        public int EdgeDistressSeverityAverage
        {
            get { return _edgeDistressSeverityAverage; }
            private set { _edgeDistressSeverityAverage = value; }
        }
       
        /// <summary>
        /// Holds average of Bleeding percentage
        /// </summary>
        public int BleedingPercentageAverage
        {
            get { return _bleedingPercentageAverage; }
            private set { _bleedingPercentageAverage = value; }
        }
        
        /// <summary>
        ///  Holds average of Bleeding severity
        /// </summary>
        public int BleedingSeverityAverage
        {
            get { return _bleedingSeverityAverage; }
            private set { _bleedingSeverityAverage = value; }
        }
        
        /// <summary>
        ///  Holds average of Corrugation percentage
        /// </summary>
        public int CorrugationPercentageAverage
        {
            get { return _corrugationPercentageAverage; }
            private set { _corrugationPercentageAverage = value; }
        }
       
        /// <summary>
        ///  Holds average of Corrugation severity
        /// </summary>
        public int CorrugationSeverityAverage
        {
            get { return _corrugationSeverityAverage; }
            private set { _corrugationSeverityAverage = value; }
        }
       
        /// <summary>
        ///  Holds average of Loss of Pavement Section percentage
        /// </summary>
        public int LossOfPavementPercentageAverage
        {
            get { return _lossOfPavementPercentageAverage; }
            private set { _lossOfPavementPercentageAverage = value; }
        }
        
        /// <summary>
        /// Holds average of Loss of Pavement Section severity
        /// </summary>
        public int LossOfPavementSeverityAverage
        {
            get { return _lossOfPavementSeverityAverage; }
            private set { _lossOfPavementSeverityAverage = value; }
        }
        
        /// <summary>
        /// Holds average of Cross Slope
        /// </summary>
        public int CrossSlopeAverage
        {
            get { return _crossSlopeAverage; }
            private set { _crossSlopeAverage = value; }
        }
       
        /// <summary>
        /// Holds average of Patches and Potholess
        /// </summary>
        public int PatchesAndPotholesAverage
        {
            get { return _patchesAndPotholesAverage; }
            private set { _patchesAndPotholesAverage = value; }
        }

        /// <summary>
        /// Holds themaximum of the rut depths.
        /// </summary>
        public int RutDepthMax
        {
            get { return _rutDepthMax; }
            private set { _rutDepthMax = value; }
        }

        /// <summary>
        /// Holds the load cracking deduct for severity level 1
        /// </summary>
        public int LoadCrackingSeverity1Deduct
        {
            get { return _loadCrackingSeverity1Deduct; }
            private set { _loadCrackingSeverity1Deduct = value; }
        }

        /// <summary>
        /// Holds the load cracking deduct for severity level 2
        /// </summary>
        public int LoadCrackingSeverity2Deduct
        {
            get { return _loadCrackingSeverity2Deduct; }
            private set { _loadCrackingSeverity2Deduct = value; }
        }

        /// <summary>
        /// Holds the load cracking deduct for severity level 3
        /// </summary>
        public int LoadCrackingSeverity3Deduct
        {
            get { return _loadCrackingSeverity3Deduct; }
            private set { _loadCrackingSeverity3Deduct = value; }
        }

        /// <summary>
        /// Holds the load cracking deduct for severity level 4
        /// </summary>
        public int LoadCrackingSeverity4Deduct
        {
            get { return _loadCrackingSeverity4Deduct; }
            private set { _loadCrackingSeverity4Deduct = value; }
        }
        /// <summary>
        /// Holds the deduct value for Rut depth.
        /// </summary>
        public int RutDepthDeduct
        {
            get { return _rutDepthDeduct; }
            private set { _rutDepthDeduct = value; }
        }

        /// <summary>
        /// Holds the deduct value for Load Cracking.
        /// </summary>
        public int LoadCrackingDeduct
        {
            get { return _loadCrackingDeduct; }
            private set { _loadCrackingDeduct = value; }
        }

        /// <summary>
        /// Holds the deduct value for Block Cracking.
        /// </summary>
        public int BlockCrackingDeduct
        {
            get { return _blockCrackingDeduct; }
            private set { _blockCrackingDeduct = value; }
        }

        /// <summary>
        /// Holds the deduct value returned for Raveling.
        /// </summary>
        public int RavelingDeduct
        {
            get { return _ravelingDeduct; }
            private set { _ravelingDeduct = value; }
        }

        /// <summary>
        /// Holds the deduct value returned for Edge Distress.
        /// </summary>
        public int EdgeDistressDeduct
        {
            get { return _edgeDistressDeduct; }
            private set { _edgeDistressDeduct = value; }
        }

        /// <summary>
        /// Holds the deduct value returned for Bleeding.
        /// </summary>
        public int BleedingDeduct
        {
            get { return _bleedingDeduct; }
            private set { _bleedingDeduct = value; }
        }

        /// <summary>
        /// Holds the deduct value returned for Loss of Pavement section.
        /// </summary>
        public int LossOfPavementDeduct
        {
            get { return _lossOfPavementDeduct; }
            private set { _lossOfPavementDeduct = value; }
        }

        /// <summary>
        /// Holds the deduct value returned for Corrugation.
        /// </summary>
        public int CorrugationDeduct
        {
            get { return _corrugationDeduct; }
            private set { _corrugationDeduct = value; }
        }
        /// <summary>
        /// Holds the deduct value returned for Reflection Cracking.
        /// </summary>
        public int ReflectionDeduct
        {
            get { return _reflectionDeduct; }
            private set { _reflectionDeduct = value; }
        }

        /// <summary>
        /// Holds the deduct value returned for Patches and potholes.
        /// </summary>
        public int PatchesAndPotholesDeduct
        {
            get { return _patchesAndPotholesDeduct; }
            private set { _patchesAndPotholesDeduct = value; }
        }

        /// <summary>
        /// Holds the deduct value returned for Cross Slope.
        /// </summary>
        public int CrossSlopeDeduct
        {
            get { return _crossSlopeDeduct; }
            private set { _crossSlopeDeduct = value; }
        }

        /// <summary>
        /// Sum of all the cracks in the project
        /// </summary>
        public int ReflectionCrackNumberSum
        {
            get { return _reflectionCrackNumberSum; }
            set { _reflectionCrackNumberSum = value; }
        }
        
        int _crossSlopeLeftSum = 0;
        /// <summary>
        /// Sum of all left cross slopes in the project
        /// </summary>
        public int CrossSlopeLeftSum
        {
            get { return _crossSlopeLeftSum; }
            set { _crossSlopeLeftSum = value; }
        }
        
        int _crossSlopeRightSum = 0;

        /// <summary>
        /// Sum of all right cross slopes in the project
        /// </summary>
        public int CrossSlopeRightSum
        {
            get { return _crossSlopeRightSum; }
            set { _crossSlopeRightSum = value; }
        }

        /// <summary>
        /// The number of segments in the project.
        /// </summary>
        public int NumberOfSegments
        {
            get { return _numberOfSegments; }
            set { _numberOfSegments = value; }
        }

        /// <summary>
        /// Total number of patches and potholes.
        /// </summary>
        public int PatchesAndPotholesSum
        {
            get { return _patchesAndPotholesSum; }
            set { _patchesAndPotholesSum = value; }
        }       

        /// <summary>
        /// Number of reflection cracking severity values.
        /// </summary>
        public int ReflectionCrackSeverityCount
        {
            get { return _reflectionCrackSeverityCount; }
            set { _reflectionCrackSeverityCount = value; }
        }

        /// <summary>
        /// Sum of all the reflection cracking severity values.
        /// </summary>
        public int ReflectionCrackSeveritySum
        {
            get { return _reflectionCrackSeveritySum; }
            set { _reflectionCrackSeveritySum = value; }
        }        

        /// <summary>
        /// Total length of all reflection cracks put together.
        /// </summary>
        public int ReflectionCrackTotalLengthSum
        {
            get { return _reflectionCrackTotalLengthSum; }
            set { _reflectionCrackTotalLengthSum = value; }
        }

        /// <summary>
        /// Total number of corrugation severity values.
        /// </summary>
        public int CorrugationSeverityCount
        {
            get { return _corrugationSeverityCount; }
            set { _corrugationSeverityCount = value; }
        }

        /// <summary>
        /// Sum of all corrugation severity values.
        /// </summary>
        public int CorrugationSeveritySum
        {
            get { return _corrugationSeveritySum; }
            set { _corrugationSeveritySum = value; }
        }

        /// <summary>
        /// Sum of all corrugation severity percentages.
        /// </summary>
        public int CorrugationPercentageSum
        {
            get { return _corrugationPercentageSum; }
            set { _corrugationPercentageSum = value; }
        }
        
        /// <summary>
        /// Sum of all loss of pavement severity values.
        /// </summary>
        public int LossOfPavementSeveritySum
        {
            get { return _lossOfPavementSeveritySum; }
            set { _lossOfPavementSeveritySum = value; }
        }

        /// <summary>
        /// Sum of all loss of pavement severity percentages.
        /// </summary>
        public int LossOfPavementPercentageSum
        {
            get { return _lossOfPavementPercentageSum; }
            set { _lossOfPavementPercentageSum = value; }
        }
        
        /// <summary>
        /// Total number of all edge distress severity values.
        /// </summary>
        public int EdgeDistressSeverityCount
        {
            get { return _edgeDistressSeverityCount; }
            set { _edgeDistressSeverityCount = value; }
        }

        /// <summary>
        /// Sum of all edge distress severity values.
        /// </summary>
        public int EdgeDistressSeveritySum
        {
            get { return _edgeDistressSeveritySum; }
            set { _edgeDistressSeveritySum = value; }
        }
        
        /// <summary>
        /// Sum of all edge distress percentages.
        /// </summary>
        public int EdgeDistressPercentageSum
        {
            get { return _edgeDistressPercentageSum; }
            set { _edgeDistressPercentageSum = value; }
        }        

        /// <summary>
        /// Total number of raveling severity values.
        /// </summary>
        public int RavelingSeverityCount
        {
            get { return _ravelingSeverityCount; }
            set { _ravelingSeverityCount = value; }
        }

        /// <summary>
        /// Sum of all raveling severity values.
        /// </summary>
        public int RavelingSeveritySum
        {
            get { return _ravelingSeveritySum; }
            set { _ravelingSeveritySum = value; }
        }
        
        /// <summary>
        /// Sum of all raveling severity percentages.
        /// </summary>
        public int RavelingPercentageSum
        {
            get { return _ravelingPercentageSum; }
            set { _ravelingPercentageSum = value; }
        }        

        /// <summary>
        /// Total number of bleeding severity values.
        /// </summary>
        public int BleedingSeverityCount
        {
            get { return _bleedingSeverityCount; }
            set { _bleedingSeverityCount = value; }
        }

        /// <summary>
        /// Sum of bleeding severity values.
        /// </summary>
        public int BleedingSeveritySum
        {
            get { return _bleedingSeveritySum; }
            set { _bleedingSeveritySum = value; }
        }

        /// <summary>
        /// sum of Bleeding percentage 
        /// </summary>
        public int BleedingPercentageSum
        {
            get { return _bleedingPercentageSum; }
            set { _bleedingPercentageSum = value; }
        }              

        /// <summary>
        /// sum of Load Cracking percentage severity level 4
        /// </summary>
        public int LoadCrackingSeverityLevel4PercentageSum
        {
            get { return _loadCrackingSeverityLevel4PercentageSum; }
            set { _loadCrackingSeverityLevel4PercentageSum = value; }
        }

        /// <summary>
        /// sum of Load Cracking percentage severity level 3
        /// </summary>
        public int LoadCrackingSeverityLevel3PercentageSum
        {
            get { return _loadCrackingSeverityLevel3PercentageSum; }
            set { _loadCrackingSeverityLevel3PercentageSum = value; }
        }

        /// <summary>
        /// sum of Load Cracking percentage severity level 2
        /// </summary>
        public int LoadCrackingSeverityLevel2PercentageSum
        {
            get { return _loadCrackingSeverityLevel2PercentageSum; }
            set { _loadCrackingSeverityLevel2PercentageSum = value; }
        }

        /// <summary>
        /// sum of Load Cracking percentage severity level 1
        /// </summary>
        public int LoadCrackingSeverityLevel1PercentageSum
        {
            get { return _loadCrackingSeverityLevel1PercentageSum; }
            set { _loadCrackingSeverityLevel1PercentageSum = value; }
        }

        /// <summary>
        /// Number of segments which have Block Cracking distress
        /// </summary>
        public int BlockCrackingSeverityCount
        {
            get { return _blockCrackingSeverityCount; }
            set { _blockCrackingSeverityCount = value; }
        }
        
        /// <summary>
        /// sum of Block Cracking severity
        /// </summary>
        public int BlockCrackingSeveritySum
        {
            get { return _blockCrackingSeveritySum; }
            set { _blockCrackingSeveritySum = value; }
        }

        /// <summary>
        /// sum of Block cracking severity
        /// </summary>
        public int BlockCrackingPercentageSum
        {
            get { return _blockCrackingPercentageSum; }
            set { _blockCrackingPercentageSum = value; }
        }

        /// <summary>
        /// sum of total rut depths inside wheel path
        /// </summary>
        public int InsideWPSum
        {
            get { return _insideWPSum; }
            set { _insideWPSum = value; }
        }
           
        /// <summary>
        ///  sum of total rut depths outside wheel path 
        /// </summary>
        public int OutsideWPSum
        {
            get { return _outsideWPSum; }
            set { _outsideWPSum = value; }
        }

        /// <summary>
        /// Rating of the Project
        /// </summary>
        public int PacesRating
        {
            get { return _pacesRating; }
            private set { _pacesRating = value; }
        }

        /// <summary>
        /// Contains a collection of segments that belong to this project.
        /// </summary>
        public Collection<Segment> SegmentsCollection
        {
            get { return _segmentsCollection; }
            private set
            {
                _segmentsCollection = value;
            }
        }
#endregion

        /// <summary>
        /// Initializes instance of Project. 
        /// </summary>
        /// <param name="segmentCollection">
        /// Collection of Segment Objects
        /// </param>
        public Project(Collection<Segment> segmentCollection)
        {
            SegmentsCollection = segmentCollection;
            CalculatePacesRating();
        }

        /// <summary>
        /// Calculates the rating of the Project based on the collection of segments provided.
        /// </summary>
        private void CalculatePacesRating()
        {
            NumberOfSegments = SegmentsCollection.Count;

            foreach (Segment segment in SegmentsCollection)
            {
                //RutDepth
                OutsideWPSum += segment.OutsideWP;               
                InsideWPSum += segment.InsideWP;
                

                //Block Cracking
                BlockCrackingPercentageSum += segment.BlockCrackingPercentage;
                if (segment.BlockCrackingSeverity != 0)
                {
                    BlockCrackingSeveritySum += segment.BlockCrackingSeverity;
                    BlockCrackingSeverityCount++;
                }

                //Load Cracking
                LoadCrackingSeverityLevel1PercentageSum += segment.LoadCrackingSeverityLevel1Percentage;
                LoadCrackingSeverityLevel2PercentageSum += segment.LoadCrackingSeverityLevel2Percentage;
                LoadCrackingSeverityLevel3PercentageSum += segment.LoadCrackingSeverityLevel3Percentage;
                LoadCrackingSeverityLevel4PercentageSum += segment.LoadCrackingSeverityLevel4Percentage;

                //Bleeding
                BleedingPercentageSum += segment.BleedingPercentage;
                if (segment.BleedingSeverity != 0)
                {
                    BleedingSeveritySum += segment.BleedingSeverity;
                    BleedingSeverityCount++;
                }

                //Raveling
                RavelingPercentageSum += segment.RavelingPercentage;
                if (segment.RavelingSeverity != 0)
                {
                    RavelingSeveritySum += segment.RavelingSeverity;
                    RavelingSeverityCount++;
                }

                //Edge Distress
                EdgeDistressPercentageSum += segment.EdgeDistressPercentage;
                if (segment.EdgeDistressSeverity != 0)
                {
                    EdgeDistressSeveritySum += segment.EdgeDistressSeverity;
                    EdgeDistressSeverityCount++;
                }

                //Loss of Pavement Section
                LossOfPavementPercentageSum += segment.LossOfPavementPercentage;
                LossOfPavementSeveritySum += segment.LossOfPavementSeverity;

                //Corrugation
                CorrugationPercentageSum += segment.CorrugationPercentage;
                if (segment.CorrugationSeverity != 0)
                {
                    CorrugationSeveritySum += segment.CorrugationSeverity;
                    CorrugationSeverityCount++;
                }
                
                //Reflection Cracking
                ReflectionCrackTotalLengthSum += segment.ReflectionCrackTotalLength;
                if (segment.ReflectionCrackSeverity != 0)
                {
                    ReflectionCrackSeveritySum += segment.ReflectionCrackSeverity;
                    ReflectionCrackSeverityCount++;
                }

                ReflectionCrackNumberSum += segment.ReflectionCrackNumber;

                //Patches and Potholes
                PatchesAndPotholesSum += segment.PatchesAndPotholes;

                // Cross Slope
                CrossSlopeLeftSum += segment.CrossSlopeLeft;
                CrossSlopeRightSum += segment.CrossSlopeRight;
            }

            LoadCrackingPercentageSeverity1Average = divide(LoadCrackingSeverityLevel1PercentageSum, NumberOfSegments);
            LoadCrackingPercentageSeverity2Average = divide(LoadCrackingSeverityLevel2PercentageSum, NumberOfSegments);
            LoadCrackingPercentageSeverity3Average = divide(LoadCrackingSeverityLevel3PercentageSum, NumberOfSegments);
            LoadCrackingPercentageSeverity4Average = divide(LoadCrackingSeverityLevel4PercentageSum, NumberOfSegments);
            BlockCrackingPercentageAverage = divide(BlockCrackingPercentageSum, NumberOfSegments);
            BlockCrackingSeverityAverage = divide(BlockCrackingSeveritySum, BlockCrackingSeverityCount);
            ReflectionNumberAverage = divide(ReflectionCrackNumberSum, NumberOfSegments);
            ReflectionLengthAverage = divide(ReflectionCrackTotalLengthSum, NumberOfSegments);
            ReflectionSeverityAverage = divide(ReflectionCrackSeveritySum, ReflectionCrackSeverityCount);
            RavelingPercentageAverage = divide(RavelingPercentageSum, NumberOfSegments);
            RavelingSeverityAverage = divide(RavelingSeveritySum, RavelingSeverityCount);
            EdgeDistressPercentageAverage = divide(EdgeDistressPercentageSum,NumberOfSegments);
            EdgeDistressSeverityAverage = divide(EdgeDistressSeveritySum, EdgeDistressSeverityCount);
            BleedingPercentageAverage = divide(BleedingPercentageSum,NumberOfSegments);
            BleedingSeverityAverage = divide(BleedingSeveritySum, BleedingSeverityCount);
            CorrugationPercentageAverage = divide(CorrugationPercentageSum, NumberOfSegments); 
            CorrugationSeverityAverage = divide(CorrugationSeveritySum, CorrugationSeverityCount);
            LossOfPavementPercentageAverage = divide(LossOfPavementPercentageSum, NumberOfSegments); 
            LossOfPavementSeverityAverage = divide(LossOfPavementSeveritySum, NumberOfSegments);
            PatchesAndPotholesAverage = divide(PatchesAndPotholesSum, NumberOfSegments);

            int[] segmentParameters = {divide(OutsideWPSum,NumberOfSegments), divide(InsideWPSum,NumberOfSegments), 
                                      LoadCrackingPercentageSeverity1Average,LoadCrackingPercentageSeverity2Average, 
                                      LoadCrackingPercentageSeverity3Average,LoadCrackingPercentageSeverity4Average, 
                                      BlockCrackingPercentageAverage,BlockCrackingSeverityAverage,
                                      ReflectionNumberAverage , ReflectionLengthAverage, 
                                      ReflectionSeverityAverage, RavelingPercentageAverage,RavelingSeverityAverage,
                                      EdgeDistressPercentageAverage,EdgeDistressSeverityAverage, 
                                      BleedingPercentageAverage,  BleedingSeverityAverage,
                                      CorrugationPercentageAverage, CorrugationSeverityAverage, 
                                      LossOfPavementPercentageAverage, LossOfPavementSeverityAverage, divide(CrossSlopeLeftSum, NumberOfSegments), 
                                      divide(CrossSlopeRightSum, NumberOfSegments),PatchesAndPotholesAverage};

            Segment testSegment = new Segment(segmentParameters);
            
            PacesRating = testSegment.SegmentRating;

            RutDepthMax = testSegment.RutDepthMax;
            CrossSlopeMax = testSegment.CrossSlopeMax;
            RutDepthDeduct = testSegment.RutDepthDeduct;
            LoadCrackingDeduct = testSegment.LoadCrackingDeduct;
            BlockCrackingDeduct = testSegment.BlockCrackingDeduct;
            BleedingDeduct = testSegment.BleedingDeduct;
            RavelingDeduct = testSegment.RavelingDeduct;
            CorrugationDeduct = testSegment.CorrugationDeduct;
            ReflectionDeduct = testSegment.ReflectionDeduct;
            LossOfPavementDeduct = testSegment.LossOfPavementDeduct;
            PatchesAndPotholesDeduct = testSegment.PatchesAndPotholesDeduct;
            EdgeDistressDeduct = testSegment.EdgeDistressDeduct;
            CrossSlopeDeduct = testSegment.CrossSlopeDeduct;

            LoadCrackingSeverity1Deduct = testSegment.LoadCrackingSeverity1Deduct;
            LoadCrackingSeverity2Deduct = testSegment.LoadCrackingSeverity2Deduct;
            LoadCrackingSeverity3Deduct = testSegment.LoadCrackingSeverity3Deduct;
            LoadCrackingSeverity4Deduct = testSegment.LoadCrackingSeverity4Deduct;
        }

        /// <summary>
        /// Calculates the ratio of the given parameters.
        /// </summary>
        /// <param name="numerator"> distress parameter</param>
        /// <param name="denominator">required number of segments</param>
        /// <returns>ratio of distress value to given number of segments</returns>
        private int divide(int numerator, int denominator)
        {
            if (denominator == 0 || numerator == 0)
                return 0;

            double tempDenominator = denominator;
            double tempNumerator = numerator;
            double highPrecisionQuotient = (tempNumerator / tempDenominator);

            int lowPrecisionQuotient = (int)highPrecisionQuotient;
            if ((lowPrecisionQuotient + 0.5) > highPrecisionQuotient)
            {
                return lowPrecisionQuotient;
            }

            return (lowPrecisionQuotient+1);
        }
    }
}