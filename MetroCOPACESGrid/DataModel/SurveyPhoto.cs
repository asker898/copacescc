﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using Newtonsoft.Json;
using Microsoft.WindowsAzure.MobileServices;

namespace MetroCOPACESGrid.DataModel
{
    public class SurveyPhoto
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "segmentSurveyId")]
        public string SegmentSurveyId { get; set; }

        [JsonProperty(PropertyName = "resourceName")]
        public string ResourceName { get; set; }

        [JsonProperty(PropertyName = "imageUri")]
        public string ImageUri { get; set; }

        // The image location
        [JsonProperty(PropertyName = "latitude")]
        public double? Latitude { get; set; }

        [JsonProperty(PropertyName = "longitude")]
        public double? Longitude { get; set; }

        [JsonProperty(PropertyName = "timeTaken")]
        public DateTime TimeTaken { get; set; }

        [Version]
        public String Version { get; set; }
    }
}
