﻿using System.Collections.ObjectModel;
using System.IO;
using Windows.Devices.Geolocation;
using Windows.Storage.Streams;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Media.Imaging;
using Blend.SampleData.SampleDataSource;
using Callisto.Controls;
using MetroCOPACESGrid.Common;
using MetroCOPACESGrid.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using MetroCOPACESGrid.Utility;
using Microsoft.WindowsAzure.MobileServices;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;

namespace MetroCOPACESGrid.ViewModels
{
    /// <summary>
    /// Used to display a single image and associate it with geoposition
    /// </summary>
    public class SurveyPhotoWrapper
    {
        public SurveyPhotoWrapper(StorageFile file)
        {
            this.File = file;
            SurveyPhoto = new SurveyPhoto();
        }

        public SurveyPhoto SurveyPhoto { get; set; }

        private StorageFile _file;
        public StorageFile File
        {
            get { return _file; }
            set { _file = value; LoadImage(); }
        }

        public BitmapImage Image { get; set; }

        private Geoposition _geoposition;
        public Geoposition Geoposition
        {
            get { return _geoposition; }
            set
            {
                _geoposition = value;

                // Save the location data in the survey table
                SurveyPhoto.Latitude = value.Coordinate.Latitude;
                SurveyPhoto.Longitude = value.Coordinate.Longitude;
            }
        }

        // Open the image _file and load it to display
        private async void LoadImage()
        {
            var bitmapImage = new BitmapImage();
            var stream = (FileRandomAccessStream)await this.File.OpenAsync(FileAccessMode.Read);
            bitmapImage.SetSource(stream);
            Image = bitmapImage;
        }
    }

    /// <summary>
    /// The class stores all the data associated with a single segment in a new survey
    /// </summary>
    public class SingleSegmentNewSurveyVm : ObservableDictionary
    {
        public SingleSegmentNewSurveyVm(Segment segment, RoadSurvey roadSurvey)
        {
            Survey = new SegmentSurvey(roadSurvey, segment);
            Segment = segment;
            EnableSaveButton = true;
            SurveyPhotoWrappers = new ObservableCollection<SurveyPhotoWrapper>();
        }

        public Segment Segment
        {
            get { return (Segment)this["Segment"]; }
            set { this["Segment"] = value; }
        }

        public bool EnableSaveButton
        {
            get { return (bool)this["EnableSaveButton"]; }
            set { this["EnableSaveButton"] = value; }
        }

        public bool IsSurveyEmpty
        {
            get
            {
                if (Survey.TreatmentYear == null &&
                    Survey.TreatmentMethod == null &&
                    Survey.LaneDirection == null &&
                    Survey.LaneNum == null &&
                    Survey.CopacesRating == 100 &&
                    Survey.SampleLocation == null &&
                    Survey.PatchPotholeNum == null)
                    return true;
                return false;
            }
        }

        public SegmentSurvey Survey
        {
            get { return this["Survey"] as SegmentSurvey; }
            set { this["Survey"] = value; }
        }

        public RoadSurvey RoadSurvey { get; set; }

        // The SurveyPhotos associated with this survey. A single survey can have multiple photos
        public ObservableCollection<SurveyPhotoWrapper> SurveyPhotoWrappers { get; set; }

        /// <summary>
        /// Save the survey to a local/remote database depending on the network connection
        /// </summary>
        public async Task Save()
        {
            await Survey.SaveToDb();

            // Get access to all the image files
            var files = SurveyPhotoWrappers.Select(e => e.File).ToList();

            // Associate the images with the survey
            // and write metadata to the image files
            foreach (var surveyPhotoWrapper in SurveyPhotoWrappers)
                await ImagingHelper.WriteSurveyPhotoMetaData(surveyPhotoWrapper.File, surveyPhotoWrapper.Geoposition, Survey.Id);

            // Save the photo files in a special location
            var localFolder = ApplicationData.Current.LocalFolder;
            StorageFolder imageFolder =
                await localFolder.TryGetItemAsync("UnuploadedImages") as StorageFolder ??
                await localFolder.CreateFolderAsync("UnuploadedImages");

            foreach (var file in files)
                await file.MoveAsync(imageFolder);

            // Doesn't allow duplicated saves
            this.EnableSaveButton = false;
        }

    }

    /// <summary>
    /// The class stores the data of all segments in a new survey
    /// </summary>
    public class NewSurveyPageVm : ObservableDictionary
    {
        // VM for single segment surveys
        public List<SingleSegmentNewSurveyVm> NewSurveyVms { get; set; }

        public RoadSurvey RoadSurvey { get; set; }

        public List<Segment> Segments
        {
            get
            {
                return ContainsKey("Segments")
                    ? this["Segments"] as List<Segment>
                    : null;
            }
            set
            {
                NewSurveyVms.Clear();
                foreach (var segment in value)
                    NewSurveyVms.Add(new SingleSegmentNewSurveyVm(segment, RoadSurvey));

                this["Segments"] = value;
            }
        }

        // The index of the selected segment
        public int CurrentSegmentIndex
        {
            get { return (int)this["CurrentSegmentIndex"]; }
            set
            {
                this["CurrentSegmentIndex"] = value;
                if (NewSurveyVms != null && NewSurveyVms.Any())
                    CurrentSegmentNewSurveyVm = NewSurveyVms[value];
            }
        }

        // The survey viewmodel corresponding to the selected segment
        public SingleSegmentNewSurveyVm CurrentSegmentNewSurveyVm
        {
            get { return this["CurrentSegmentNewSurveyVm"] as SingleSegmentNewSurveyVm; }
            set { this["CurrentSegmentNewSurveyVm"] = value; }
        }

        public NewSurveyPageVm()
        {
            NewSurveyVms = new List<SingleSegmentNewSurveyVm>();
            CurrentSegmentIndex = 0;
        }
    }
}
