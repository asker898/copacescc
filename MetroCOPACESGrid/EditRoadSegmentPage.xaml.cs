﻿using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Reflection;
using Windows.Graphics.Printing.OptionDetails;
using MetroCOPACESGrid.Common;
using MetroCOPACESGrid.DataModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Item Detail Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234232

namespace MetroCOPACESGrid
{
    /// <summary>
    /// A page that displays details for a single item within a group.
    /// </summary>
    public sealed partial class EditRoadSegmentPage : Page
    {
        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();

        /// <summary>
        /// NavigationHelper is used on each page to aid in navigation and 
        /// process lifetime management
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        public EditRoadSegmentPage()
        {
            this.InitializeComponent();
            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += navigationHelper_LoadState;
        }

        private void navigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            var item = e.NavigationParameter;
            var typeInfo = item.GetType().GetTypeInfo();

            DefaultViewModel["Item"] = item;

            DefaultViewModel["Properties"] =
                typeInfo.DeclaredProperties
                .Where(prop => !prop.IsDefined(typeof(InternalAttribute)))
                .Select(prop => new
                {
                    Name = AttributeHelper.GetDisplayName(prop),
                    PropertyInfo = prop,
                    Value = prop.GetValue(item),
                    Options = AttributeHelper.GetOptions(prop),
                    IsReadOnly = prop.IsDefined(typeof(ReadOnlyAttribute))
                }).ToList();
        }

        #region NavigationHelper registration

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedFrom(e);
        }

        #endregion
    }
}