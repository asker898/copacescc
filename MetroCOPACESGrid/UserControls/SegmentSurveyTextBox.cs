﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.ServiceModel.Channels;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using MetroCOPACESGrid.DataModel;
using MetroCOPACESGrid.Utility;
using MetroCOPACESGrid.Utility.Exceptions;

// The Templated Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234235

namespace MetroCOPACESGrid.UserControls
{
    public sealed class SegmentSurveyTextBox : TextBox
    {
        public PropertyInfo PropertyInfo
        {
            get
            {
                var propName = Tag as String;
                return Survey != null ? Survey.GetType().GetTypeInfo().GetDeclaredProperty(propName) : null;
            }
        }

        public SegmentSurvey Survey
        {
            get
            {
                return DataContext as SegmentSurvey;
            }
        }

        public bool IsDataValid { get; set; }

        public SegmentSurveyTextBox()
        {
            DefaultStyleKey = typeof(TextBox);

            FontFamily = new FontFamily("Segoe UI");
            FontSize = 15;
            Margin = new Thickness(0, 0, 10, 5);

            LostFocus += TextBox_OnLostFocus;
            DataContextChanged += TextBox_OnDataContextChanged;
            Loaded += TextBox_OnLoaded;

            IsDataValid = true;
        }

        private void TextBox_OnLoaded(object sender, RoutedEventArgs e)
        {
            // "Tag" property can be obtained from there
            var scope = new InputScope();
            var propName = Tag as String;

            if (PropertyInfo == null)
                return;

            // Set input scope
            if (PropertyInfo.PropertyType == typeof(String))
                scope.Names.Add(new InputScopeName { NameValue = InputScopeNameValue.AlphanumericFullWidth });
            else
                scope.Names.Add(new InputScopeName { NameValue = InputScopeNameValue.Number });
            InputScope = scope;

            // Set header
            var display = PropertyInfo.GetCustomAttribute<DisplayAttribute>();
            Header = display != null ? display.Name : propName;
        }

        private void TextBox_OnDataContextChanged(FrameworkElement sender, DataContextChangedEventArgs args)
        {
            var propValue = PropertyInfo.GetValue(Survey);
            Text = propValue != null ? propValue.ToString() : "";
        }

        private void TextBox_OnLostFocus(object sender, RoutedEventArgs e)
        {
            // Do the data validation check here
            try
            {
                if (String.IsNullOrEmpty(Text))
                {
                    PropertyInfo.SetValue(Survey, null);
                    IsDataValid = true;
                    Background = Application.Current.Resources["TextBoxBackgroundThemeBrush"] as SolidColorBrush;
                }
                else
                {
                    // Validate data 
                    var range = AttributeHelper.GetRange(PropertyInfo);
                    if (range != null)
                    {
                        var newValue = Convert.ToDouble(Text);
                        if (range.Min >= newValue || range.Max < newValue)
                            throw new DataOutOfRangeException();

                        Background = Application.Current.Resources["TextBoxBackgroundThemeBrush"] as SolidColorBrush;
                        IsDataValid = true;
                    }

                    // Convert to the property data type
                    if (PropertyInfo.PropertyType == typeof(String))
                    {
                        PropertyInfo.SetValue(Survey, Text);
                    }
                    else if (PropertyInfo.PropertyType == typeof(double?) ||
                             PropertyInfo.PropertyType == typeof(double))
                    {
                        var newValue = Convert.ChangeType(Text, typeof(double));
                        PropertyInfo.SetValue(Survey, newValue);
                    }
                    else if (
                        PropertyInfo.PropertyType == typeof(int?) ||
                        PropertyInfo.PropertyType == typeof(int))
                    {
                        var newValue = Convert.ChangeType(Text, typeof(int));
                        PropertyInfo.SetValue(Survey, newValue);
                    }
                    IsDataValid = true;
                }
            }
            catch (DataOutOfRangeException)
            {
                Background = new SolidColorBrush(Colors.Red);
                PropertyInfo.SetValue(Survey, null);
                IsDataValid = false;
                NotificationHelper.ShowErrorMessage("The data is out of range!");
            }
            catch (Exception)
            {
                Background = new SolidColorBrush(Colors.Red);
                PropertyInfo.SetValue(Survey, null);
                IsDataValid = false;
                NotificationHelper.ShowErrorMessage("The data input format is wrong!");
            }
            finally
            {
                if (Survey != null)
                    Survey.CalculateRating();
            }
        }
    }
}
