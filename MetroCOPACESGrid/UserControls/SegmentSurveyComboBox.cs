﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Documents;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using MetroCOPACESGrid.DataModel;

// The Templated Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234235

namespace MetroCOPACESGrid.UserControls
{
    public class SegmentSurveyComboBox : ComboBox
    {
        public PropertyInfo PropertyInfo
        {
            get
            {
                var propName = Tag as String;
                return Survey != null ? Survey.GetType().GetTypeInfo().GetDeclaredProperty(propName) : null;
            }
        }

        public SegmentSurvey Survey
        {
            get
            {
                return DataContext as SegmentSurvey;
            }
        }

        public SegmentSurveyComboBox()
        {
            this.DefaultStyleKey = typeof(ComboBox);

            SelectionChanged += ComboBox_OnSelectionChanged;
            DataContextChanged += ComboBox_OnDataContextChanged;

            FontFamily = new FontFamily("Segoe UI");
            FontSize = 15;
            Margin = new Thickness(0, 2, 10, 5);

            Loaded += ComboBox_OnLoaded;
        }

        private void ComboBox_OnLoaded(object sender, RoutedEventArgs e)
        {

            var propName = Tag as String;

            // Set header
            var display = PropertyInfo.GetCustomAttribute<DisplayAttribute>();
            Header = display != null ? display.Name : propName;
        }

        private void ComboBox_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (SelectedValue != null)
            {
                if ((string)SelectedValue == " ")
                {
                    PropertyInfo.SetValue(Survey, null);
                }
                else
                {
                    var valueString = SelectedValue as string;
                    if (PropertyInfo.PropertyType == typeof(String))
                    {
                        PropertyInfo.SetValue(Survey, valueString);
                    }
                    else if (PropertyInfo.PropertyType == typeof(double?) ||
                             PropertyInfo.PropertyType == typeof(double))
                    {
                        var newValue = Convert.ChangeType(valueString, typeof(double));
                        PropertyInfo.SetValue(Survey, newValue);
                    }
                    else if (
                        PropertyInfo.PropertyType == typeof(int?) ||
                        PropertyInfo.PropertyType == typeof(int))
                    {
                        var newValue = Convert.ChangeType(valueString, typeof(int));
                        PropertyInfo.SetValue(Survey, newValue);
                    }
                }
            }
            if (Survey != null)
                Survey.CalculateRating();
        }

        private void ComboBox_OnDataContextChanged(FrameworkElement sender, DataContextChangedEventArgs args)
        {
            ItemsSource = AttributeHelper.GetOptions(PropertyInfo);
            var propValue = PropertyInfo.GetValue(Survey);
            SelectedValue = propValue == null ? null : propValue.ToString();
        }
    }
}
