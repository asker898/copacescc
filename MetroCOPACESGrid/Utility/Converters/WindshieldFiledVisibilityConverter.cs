﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace MetroCOPACESGrid.Utility.Converters
{
    public class WindshieldFiledVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            // When "IsWindshield" is true
            if ((bool)value == true)
            {
                return (String)parameter == "WindshieldField" ? Visibility.Visible : Visibility.Collapsed;
            }
            else
            {
                return (String)parameter == "WindshieldField" ? Visibility.Collapsed : Visibility.Visible;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
