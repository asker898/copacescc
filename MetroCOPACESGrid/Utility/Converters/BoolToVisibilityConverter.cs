﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace MetroCOPACESGrid.Utility.Converters
{
    public class BoolToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (parameter == null)
                return (bool)value ? Visibility.Visible : Visibility.Collapsed;
            if ((String)parameter == "Reverse")
                return (bool)value ? Visibility.Collapsed : Visibility.Visible;
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
