﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Geolocation;
using Windows.Foundation;
using Windows.Graphics.Imaging;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Core;
using Windows.UI.Xaml;
using MetroCOPACESGrid.DataModel;

namespace MetroCOPACESGrid.Utility
{
    public static class ImagingHelper
    {
        /// <summary>
        /// Translate from decimal lat/lng values to an int array of [degree, minute, second]
        /// </summary>
        public static int[] CoordDoubleToIntArray(double coord)
        {
            coord = Math.Abs(coord);
            var sec = (int)Math.Round(coord * 3600);
            int deg = sec / 3600;
            sec = Math.Abs(sec % 3600);
            int min = sec / 60;
            sec = (int)Math.Round((coord - (deg + (min / 60.0))) * 3600 * 10000);

            return new [] {deg, min, sec}.Select(Math.Abs).ToArray();
        }

        public static double CoordIntArrayToDouble(double[] coords)
        {
            if (coords == null || coords.Count() < 3)
                return 0;
            return coords[0] + (coords[1] / 60.0) + (coords[2]/3600.0);
        }

        /// <summary>
        /// Write the location metadata to the image file
        /// </summary>
        public async static Task WriteSurveyPhotoMetaData(StorageFile file, Geoposition pos, string surveyId = "")
        {
            using (IRandomAccessStream stream = await file.OpenAsync(FileAccessMode.ReadWrite))
            {
                var decoder = await BitmapDecoder.CreateAsync(stream);
                var encoder = await BitmapEncoder.CreateForTranscodingAsync(stream, decoder);
                
                // Write the gps data
                var propertySet = new BitmapPropertySet();
                var latitudeNumerator = new BitmapTypedValue(CoordDoubleToIntArray(pos.Coordinate.Latitude), PropertyType.Int32Array);
                var latitudeRef = new BitmapTypedValue("N", PropertyType.String);
                var longitudeNumerator = new BitmapTypedValue(CoordDoubleToIntArray(pos.Coordinate.Longitude), PropertyType.Int32Array);
                var longitudeRef = new BitmapTypedValue("W", PropertyType.String);

                propertySet.Add("System.GPS.LatitudeNumerator", latitudeNumerator);
                propertySet.Add("System.GPS.LatitudeRef", latitudeRef);
                propertySet.Add("System.GPS.LatitudeDenominator", new BitmapTypedValue(new [] { 1, 1, 10000 }, PropertyType.Int32Array));

                propertySet.Add("System.GPS.LongitudeNumerator", longitudeNumerator);
                propertySet.Add("System.GPS.LongitudeRef", longitudeRef);
                propertySet.Add("System.GPS.LongitudeDenominator", new BitmapTypedValue(new[] { 1, 1, 10000 }, PropertyType.Int32Array));

                // Write the surveyId data
                if (!string.IsNullOrEmpty(surveyId))
                {
                    var surveyIdTyped = new BitmapTypedValue(surveyId, Windows.Foundation.PropertyType.String);
                    propertySet.Add("System.Comment", surveyIdTyped);
                }

                await encoder.BitmapProperties.SetPropertiesAsync(propertySet);
                await encoder.FlushAsync();
            }
        }

        /// <summary>
        /// Read the location metadata from an image file
        /// </summary>
        /// <returns>A list of double values, the first element is latitude, the second is longitude</returns>
        public async static Task<SurveyPhoto> ReadImageMetaData(StorageFile file)
        {
            var requests = new List<string> { "System.GPS.Latitude", "System.GPS.Longitude", "System.Comment", "System.Photo.DateTaken" };
            var retrievedProps = await file.Properties.RetrievePropertiesAsync(requests);
            var res = new SurveyPhoto {ResourceName = file.Name};

            foreach (var request in requests.Where(retrievedProps.ContainsKey))
            {
                switch (request)
                {
                    case "System.GPS.Latitude":
                        res.Latitude = CoordIntArrayToDouble(retrievedProps[request] as double[]); break;
                    case "System.GPS.Longitude":
                        res.Longitude = -CoordIntArrayToDouble(retrievedProps[request] as double[]); break;
                    case "System.Photo.DateTaken":
                        res.TimeTaken = ((DateTimeOffset)retrievedProps[request]).DateTime; break;
                    case "System.Comment":
                    default:
                        res.SegmentSurveyId = retrievedProps[request] as string; break;
                }
            }
            return res;
        }
    }
}
