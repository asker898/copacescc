﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetroCOPACESGrid.Utility
{
    public static class GlobalPreferences
    {
        public static bool CopyFromLastSurvey { get; set; }
        public static bool CopyFromLastSegment { get; set; }
    }
}
