﻿using MetroCOPACESGrid.DataModel;
using Microsoft.WindowsAzure.MobileServices;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace MetroCOPACESGrid.Utility
{
    public static class AzureQueryUtil
    {
        /// <summary>
        /// Load all the query results regardless of the paging limit on the server
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="table"></param>
        /// <param name="bufferSize"></param>
        /// <returns></returns>
        public async static Task<List<T>> LoadAllAsync<T>(this IMobileServiceTableQuery<T> table, int bufferSize = 1000)
        {
            var query = table.IncludeTotalCount();
            var results = await query.ToEnumerableAsync();
            long count = ((ITotalCountProvider)results).TotalCount;
            if (results != null && count > 0)
            {
                var updates = new List<T>();
                while (updates.Count < count)
                {
                    var next = await query.Skip(updates.Count).Take(bufferSize).ToListAsync();
                    updates.AddRange(next);
                }
                return updates;
            }

            return null;
        }
    }
}
