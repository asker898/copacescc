﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using System.Web.Http.Results;
using CityCountyService.Utility;
using Microsoft.WindowsAzure.Mobile.Service;
using CityCountyService.Models;
using CityCountyService.DataObjects;

namespace CityCountyService.Controllers
{
    public class RoadController : TableController<Road>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            CopacesCcDbContext context = new CopacesCcDbContext();
            DomainManager = new EntityDomainManager<Road>(context, Request, Services);
        }

        [Queryable(MaxTop = 1000)]
        // GET tables/Road
        public IQueryable<Road> GetAllRoad()
        {
            return Query(); 
        }

        // GET tables/Road/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<Road> GetRoad(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/Road/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<Road> PatchRoad(string id, Delta<Road> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/Road/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public async Task<IHttpActionResult> PostRoad(Road item)
        {
            if (item.Id == null)
                return null;

            var place = RelationHelper.FindPlaceByRoad(item);
            item.PlaceId = place != null ? place.Id : null;

            Road road = await InsertAsync(item);

            return CreatedAtRoute("Tables", new { id = road.Id }, road);
        }

        // DELETE tables/Road/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteRoad(string id)
        {
             return DeleteAsync(id);
        }

    }
}