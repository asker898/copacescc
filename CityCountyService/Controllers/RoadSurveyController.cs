﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.WindowsAzure.Mobile.Service;
using CityCountyService.Models;
using CityCountyService.DataObjects;

namespace CityCountyService.Controllers
{
    public class RoadSurveyController : TableController<RoadSurvey>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            CopacesCcDbContext context = new CopacesCcDbContext();
            DomainManager = new EntityDomainManager<RoadSurvey>(context, Request, Services);
        }

        [Queryable(MaxTop = 1000)]
        // GET tables/RoadSurvey
        public IQueryable<RoadSurvey> GetAllRoadSurvey()
        {
            return Query(); 
        }

        // GET tables/RoadSurvey/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<RoadSurvey> GetRoadSurvey(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/RoadSurvey/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<RoadSurvey> PatchRoadSurvey(string id, Delta<RoadSurvey> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/RoadSurvey/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public async Task<IHttpActionResult> PostRoadSurvey(RoadSurvey item)
        {
            RoadSurvey current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/RoadSurvey/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteRoadSurvey(string id)
        {
             return DeleteAsync(id);
        }

    }
}