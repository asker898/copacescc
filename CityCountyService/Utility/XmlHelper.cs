﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using CityCountyService.Models;
using CityCountyService.DataObjects;

namespace CityCountyService.Utility
{
    public static class XmlHelper
    {
        public static List<Segment> GetSegmentsFromXml(String xmlFilePath)
        {
            var xmlRoot = XElement.Load(xmlFilePath);
            return
                (from segment in xmlRoot.Elements("Segment")
                 select new Segment
                 {
                     SequenceId = (int)segment.Element("Sequence_ID"),
                     InventId = (int)segment.Element("INVENT_ID"),
                     RoadId = (String)segment.Element("RoadID"),
                     From = (String)segment.Element("Segment_From"),
                     To = (String)segment.Element("Segment_To"),
                     Length = (double?)segment.Element("Length")
                 }).ToList();
        }

        public static List<RoadSurvey> GetRoadSurveysFromXml(String xmlFilePath)
        {
            var xmlRoot = XElement.Load(xmlFilePath);
            return
                (from roadSurvey in xmlRoot.Elements("RoadSurvey")
                 select new RoadSurvey
                 {
                     RoadId = (String)roadSurvey.Element("RoadID"),
                     Date = (DateTime)roadSurvey.Element("Road_Survey_Date"),
                     ProjectRating = (double?)roadSurvey.Element("Project_Rating"),
                     RutAvg = (double?)roadSurvey.Element("Rut_Avg"),
                     RutDeduct = (double?)roadSurvey.Element("Rut_Deduct"),
                     LoadLevel1Avg = (double?)roadSurvey.Element("Load_Sev1_Avg"),
                     LoadLevel1Deduct = (double?)roadSurvey.Element("Load_Sev1_Deduct"),
                     LoadLevel2Avg = (double?)roadSurvey.Element("Load_Sev2_Avg"),
                     LoadLevel2Deduct = (double?)roadSurvey.Element("Load_Sev2_Deduct"),
                     LoadLevel3Avg = (double?)roadSurvey.Element("Load_Sev3_Avg"),
                     LoadLevel3Deduct = (double?)roadSurvey.Element("Load_Sev3_Deduct"),
                     LoadLevel4Avg = (double?)roadSurvey.Element("Load_Sev4_Avg"),
                     LoadLevel4Deduct = (double?)roadSurvey.Element("Load_Sev4_Deduct"),
                     BlockLevel = (double?)roadSurvey.Element("Block_Sev"),
                     BlockAvg = (double?)roadSurvey.Element("Block_Avg"),
                     BlockDeduct = (double?)roadSurvey.Element("Block_Deduct"),
                     ReflectLevel = (double?)roadSurvey.Element("Reflect_Sev"),
                     ReflectAvg = (double?)roadSurvey.Element("Reflect_Avg"),
                     ReflectDeduct = (double?)roadSurvey.Element("Reflect_Deduct"),
                     EdgeLevel = (double?)roadSurvey.Element("Edge_Sev"),
                     EdgeAvg = (double?)roadSurvey.Element("Edge_Avg"),
                     EdgeDeduct = (double?)roadSurvey.Element("Edge_Deduct"),
                     RavelAvg = (double?)roadSurvey.Element("Ravel_Avg"),
                     RavelDeduct = (double?)roadSurvey.Element("Ravel_Deduct"),
                     RavelLevel = (double?)roadSurvey.Element("Ravel_Sev"),
                     CorrugAvg = (double?)roadSurvey.Element("Corrug_Avg"),
                     CorrugLevel = (double?)roadSurvey.Element("Corrug_Sev"),
                     CorrugDeduct = (double?)roadSurvey.Element("Corrug_Deduct"),
                     LossLevel = (double?)roadSurvey.Element("Loss_Sev"),
                     LossAvg = (double?)roadSurvey.Element("Loss_Avg"),
                     LossDeduct = (double?)roadSurvey.Element("Loss_Deduct"),
                     PatchAvg = (double?)roadSurvey.Element("Rut_Avg"),
                     PatchDeduct = (double?)roadSurvey.Element("Patch_Deduct"),
                     BleedAvg = (double?)roadSurvey.Element("Bleed_Avg"),
                     BleedDeduct = (double?)roadSurvey.Element("Bleed_Deduct"),
                     BleedLevel = (double?)roadSurvey.Element("Bleed_Sev")
                 }).ToList();
        }

        public static List<SegmentSurvey> GetSegmentSurveysFromXml(String xmlFilePath)
        {
            var xmlRoot = XElement.Load(xmlFilePath);
            return
                (from segSurvey in xmlRoot.Elements("SegSurvey")
                 select new SegmentSurvey()
                 {
                     RoadSurveyDate = (DateTime)segSurvey.Element("Road_Survey_Date"),
                     SegSurveyDate = (DateTime)segSurvey.Element("Survey_Date"),
                     InventId = (int)segSurvey.Element("INVENT_ID"),
                     SequenceId = (int)segSurvey.Element("Sequence_ID"),
                     RoadId = (string)segSurvey.Element("RoadID"),
                     CopacesRating = (double?)segSurvey.Element("COPACES_Rating"),
                     Rater = (string)segSurvey.Element("RaterTG"),
                     TreatmentYear = (string)segSurvey.Element("Treatment_Year"),
                     TreatmentMethod = (string)segSurvey.Element("Treatment_Method"),
                     LaneDirection = (string)segSurvey.Element("Lane_Direction"),
                     LaneNum = (string)segSurvey.Element("Lane_No"),
                     SampleLocation = (string)segSurvey.Element("Sample_Location"),
                     RutOutWp = (double?)segSurvey.Element("Rut_Out_WP"),
                     RutInWp = (double?)segSurvey.Element("Rut_In_WP"),
                     LoadLevel1 = (double?)segSurvey.Element("Load_Lev1"),
                     LoadLevel2 = (double?)segSurvey.Element("Load_Lev2"),
                     LoadLevel3 = (double?)segSurvey.Element("Load_Lev3"),
                     LoadLevel4 = (double?)segSurvey.Element("Load_Lev4"),
                     BlockPercentage = (double?)segSurvey.Element("Block_Pct"),
                     BlockLevel = (int?) segSurvey.Element("Block_Lev"),
                     ReflectNum = (double?)segSurvey.Element("Reflect_No"),
                     ReflectLength = (double?)segSurvey.Element("Reflect_Len"),
                     ReflectLevel = (int?) segSurvey.Element("Reflect_Lev"),
                     RavelPercentage = (double?)segSurvey.Element("Ravel_Pct"),
                     RavelLevel = (int?) segSurvey.Element("Ravel_Lev"),
                     EdgePercentage = (double?)segSurvey.Element("Edge_Pct"),
                     EdgeLevel = (int?) segSurvey.Element("Edge_Lev"),
                     BleedPercentage = (double?)segSurvey.Element("Bleed_Pct"),
                     BleedLevel = (int?) segSurvey.Element("Bleed_Lev"),
                     CorrugPercentage = (double?)segSurvey.Element("Corrug_Pct"),
                     CorrugLevel = (int?) segSurvey.Element("Corrug_Lev"),
                     LossPavPercentage = (double?)segSurvey.Element("Loss_Pave_Pct"),
                     LossPavLevel = (int?) segSurvey.Element("Loss_Pave_Lev"),
                     CrossSlopeLeft = (double?)segSurvey.Element("Cross_Slope_Left"),
                     CrossSlopeRight = (double?)segSurvey.Element("Cross_Slope_Right"),
                     PatchPotholeNum = (int?)segSurvey.Element("Patch_pothole"),
                     IsWindshieldSurvey = (string)segSurvey.Element("IsWindshield_SurveyNO") == "YES",
                     Remarks = (string)segSurvey.Element("Comments")
                 }).ToList();
        }
    }
}