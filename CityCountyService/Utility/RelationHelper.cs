﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CityCountyService.Models;
using CityCountyService.DataObjects;

namespace CityCountyService.Utility
{
    public static class RelationHelper
    {
        public static Place FindPlaceByRoad(Road road)
        {
            using (var context = new CopacesCcDbContext())
            {
                //var fullPlaceName = road.Jurisdiction;
                //var placeName = fullPlaceName
                //    .Replace("COUNTY", "")
                //    .Replace("CITY", "")
                //    .Trim();

                //var matchPlace = context.Places.Where(p => p.Name == placeName).ToList();
                //if (!matchPlace.Any())
                //    return null;

                //// Sometimes a city and a county may have the same name,
                //// like Newton County and Newton City, so futher filtering
                //// is needed
                //Place place;
                //if (matchPlace.Count > 1)
                //{
                //    var type = fullPlaceName.ToUpper().Contains("COUNTY") ? "County" : "City";
                //    place = matchPlace.First(p => p.Type == type);
                //}
                //else
                //    place = matchPlace.First();
                var placeName = road.Jurisdiction;
                var placeType = road.PlaceType;
                var findQuery = context.Places.Where(p => p.Name == placeName && p.Type == placeType);
                if (findQuery.Any())
                    return findQuery.First();
                return null;
            }
        }

        public static Place FindPlaceByJurisdiction(String ju, String type)
        {
            return FindPlaceByRoad(new Road {Jurisdiction = ju, PlaceType = type});
        }

        public static Road FindRoadBySegment(CopacesCcDbContext context, Segment segment)
        {
            var roadId = segment.RoadId;
            var matchRoad = context.Roads.Where(r => r.Id == roadId).ToList();
            if (!matchRoad.Any())
                return null;

            return matchRoad.First();
        }

        public static Place FindPlaceBySegment(CopacesCcDbContext context, Segment segment)
        {
            var road = FindRoadBySegment(context, segment);
            if (road == null)
                return null;

            return FindPlaceByRoad(road);
        }

        public static void ProcessNewRoad(Road road)
        {
            var place = FindPlaceByRoad(road);
            if (place != null)
                place.Roads.Add(road);
        }

        public static void ProcessNewSegment(Segment segment)
        {
            //var road = FindRoadBySegment(new CopacesCcDbContext(), segment);
            //if (road != null)
            //    road.Segments.Add(segment);

            //var place = FindPlaceByRoad(road);
            //if (place != null)
            //    place.Segments.Add(segment);
        }
    }
}