# Testing Schedule
## Tablet App
### Before start new road survey
- download initial data (offline / online) [Done]
- start survey without initial data (offline / online) [Done]
- start survey with initial data [Done]
- search for road (offline / online) [Done]
- start new road survey [Done]
   - on road with 1 segment [Done]
   - on road with > 1 segment [Done]
- continue unfinished survey [Done]

### Create new survey
- save empty survey [Done]
- save a survey with photo [Done]
- save a survey with photo (with satelite location available / unavailable) [Done]
- **save a survey with multiple photos**
- switch among multiple segment surveys, check if the unsaved surveys are cached [Done]
- go back before saving, to see if the content gets saved somewhere or any notification windows pops out [Done]

### After finish adding segment survey info
- check the status of the road that just been surveyed [Done]
	- only 1 segment -> `road.HasUnfinishedSurvey` should be false [Done]
	- more than 1 segments -> `road.HasUnfinishedSurvey` should be true [Done]
- check if the `road.LastSurveyedDate` gets updates properly [Done]

### Continue unfinished survey
- only show unsurveyed segments [Done]
- if inconsistency happens (no unsurveyed segments but the roadSurvey is not marked as complete), then handle it correctly

### Edit segment survey
- show existing survey data [Done]
- notification upon saving status (successful or not) [Done]

### Sync
- sync after creating survey with photo(s) [Done]
- sync after editing survey  [Done]
- sync after changes made on the server [Done]

## Website

## Mobile Service