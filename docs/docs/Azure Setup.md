# Azure Setup
## Step 0:
Register for an Microsoft account, and create an azure subscription.

## Step 1:
Go to [Azure Management Size](https://manage.windowsazure.com), and that is the hub for all Azure services. 

For Mobile Service:
Create an new mobile service by click the "Add New" button at the bottom left corner, choose "Mobile Service", then choose "Create";
Then you need to either create a new database, make sure the region is chose as "East US". For the Backend, select ".NET".
![](http://i.imgur.com/zTgfRfR.png)

After you created an new Mobile Service instance, you can get the credential key to the service by click the "Manage Keys" button located at the bottom bar. 
![](http://i.imgur.com/2dStfWI.png)

Then to publish this new Mobile Service instance, you can right click your Mobile Service project in Visual Studio and choose "Publish", then log in your Azure account, select the new instance that you just created, and then click on "Publish";

To use your new Mobile Service instance, you need to replace the key string in the `App.xaml.cs` file. The key string is contained in the following code:

```csharp
public static MobileServiceClient MobileService = new MobileServiceClient(
    "your mobile service url",
    "your mobile service key string"
    )
{
    SerializerSettings = new MobileServiceJsonSerializerSettings
    {
        ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
    }
};
```

For the website, the creating process is similar to the Mobile Service. Please use a different database with the Mobile Service though. And the publish process is also similar. 

Another azure service needed is the storage service. All the non-tabular files like images, videos etc are stored in the Azure Blob Storage. The mechanism of how that works can be referred to this [tutorial](http://azure.microsoft.com/en-us/documentation/articles/mobile-services-dotnet-backend-windows-store-dotnet-upload-data-blob-storage/). 