﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using Copaces.CityCounty.Website.Models;
using Copaces.CityCounty.Website.Models.DataObjects;
using Copaces.CityCounty.Website.Utility;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using PagedList;

namespace Copaces.CityCounty.Website.Controllers
{
    public class SegmentsController : Controller
    {
        /// <summary>
        /// Application DB context
        /// </summary>
        ApplicationDbContext ApplicationDbContext { get; set; }

        /// <summary>
        /// User manager - attached to application DB context
        /// </summary>
        UserManager<ApplicationUser> UserManager { get; set; }

        private ApplicationUser CurrentUser {
            get {
                return User.Identity.IsAuthenticated ? UserManager.FindById(User.Identity.GetUserId()) : null;
            }
        }

        public SegmentsController()
        {
            ApplicationDbContext = new ApplicationDbContext();
            UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(this.ApplicationDbContext));
        }

        // GET: Segments
        //public async Task<ActionResult> Index(int? pageNum)
        //{
        //    if (User.Identity.IsAuthenticated == false)
        //        //return View();
        //        return RedirectToAction("Login", "Account");


        //    var segments = await ApiHelper.GetSegmentsByPlace(new Place() {Id = CurrentUser.PlaceId});

        //    var pageNumOrDefault = pageNum ?? 1;
        //    const int countPerPage = 50;

        //    return View(segments.ToPagedList(pageNumOrDefault, countPerPage));
        //}

        public ActionResult Index(string id){
            return RedirectToAction("RoadSegment", "RoadSegment", new { id = id });
        }
        // GET: Segments/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Segment segment = await ApiHelper.GetSegmentById(id);
            if (segment == null)
            {
                return HttpNotFound();
            }
            List<SegmentSurvey> surys = await ApiHelper.GetSegmentSurveysBySegment(segment);
            SegmentSurveyViewModel data = new SegmentSurveyViewModel(surys, segment);
            return View(data);
        }



        public ViewResult Create(string RoadId)
        {
            Segment seg = new Segment();
            seg.RoadId = RoadId;
            return View(seg);
        }


        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var segment = await ApiHelper.GetSegmentById(id);
            if (segment == null)
            {
                return HttpNotFound();
            }
            return View(segment);
        }

        // POST: Roads/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            //var road = await db.Roads.FindAsync(id);
            //db.Roads.Remove(road);
            //await db.SaveChangesAsync();
            await ApiHelper.DeleteSegmentByID(id);
            return RedirectToAction("Index");
        }







        // POST: Segments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(
            [Bind(
                Include =
                    "From,To,HasCurbOrGutter,Id,RoadId,SequenceId,LaneDirection,LaneNum,Length,Remarks,SampleLocation"
                )] Segment segment)
        {
            if (ModelState.IsValid)
            {
                //db.Segments.Add(segment);
                //await db.SaveChangesAsync();
                //segment.Id = ApiHelper.setSegmentId();
                var curUser = UserManager.FindById(User.Identity.GetUserId());
                //segment.Place = ApiHelper.GetPlaceById(curUser.PlaceId).Result;
                segment.PlaceId = curUser.PlaceId;
                var res = await ApiHelper.GetNextSegmentInventId(curUser.PlaceId);
                segment.InventId = res+1;
                segment.Id = ApiHelper.GetNextRoadId();
                await ApiHelper.CreateSegment(segment);
                return RedirectToAction("RoadSegments", "RoadSegments", new { id = segment.RoadId });
            }
               
            ModelStateErrors(ModelState);           // For debug purpose
            ViewBag.RoadId = new SelectList(await ApiHelper.GetRoadsByPlaceId(CurrentUser.PlaceId), "Id", "District");
            return View(segment);
        }


        public static void ModelStateErrors(ModelStateDictionary modelState)
        {
            var errors = modelState.Where(a => a.Value.Errors.Count > 0)
                .Select(b => new { b.Key, b.Value.Errors })
                .ToArray();

            foreach (var modelStateErrors in errors)
            {
                System.Diagnostics.Debug.WriteLine("...Errored When Binding.", modelStateErrors.Key.ToString());

            }

        }
        // GET: Segments/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Segment segment = await ApiHelper.GetSegmentById(id);
            if (segment == null)
            {
                return HttpNotFound();
            }
            ViewBag.RoadId = new SelectList(await ApiHelper.GetRoadsByPlaceId(CurrentUser.PlaceId), "Id", "District");
            return View(segment);
        }

        // POST: Segments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(
            [Bind(
                Include =
                    "Id,From,HasCurbOrGutter,InventId,LaneDirection,LaneNum,Length,PlaceId,Remarks,RoadId,SampleLocation,SequenceId,To"
                )] Segment segment)
        {
            if (ModelState.IsValid)
            {
                //db.Entry(segment).State = EntityState.Modified;
                //await db.SaveChangesAsync();
                var curSegment = ApiHelper.GetSegmentById(segment.Id).Result;
                curSegment.SequenceId = segment.SequenceId;
                curSegment.From = segment.From;
                curSegment.To = segment.To;
                curSegment.LaneDirection = segment.LaneDirection;
                curSegment.LaneNum = segment.LaneNum;
                curSegment.Length = segment.Length;
                curSegment.SampleLocation = segment.SampleLocation;
                curSegment.HasCurbOrGutter = segment.HasCurbOrGutter;
                curSegment.Remarks = segment.Remarks;
                await ApiHelper.EditSegment(curSegment);
                return RedirectToAction("Index");
            }
            //ViewBag.RoadId = new SelectList(await ApiHelper.GetRoadsByPlaceId(CurrentUser.PlaceId), "Id", "District");

            return View(segment);
        }

        // GET: Segment/History/5
        public async Task<ActionResult> History(int? id)
        {
            if (!id.HasValue)
                return View();

            var surveyList = await ApiHelper.GetSegmentSurveysBySegment(new Segment {InventId = id.Value});

            if (surveyList == null || surveyList.Count == 0)
            {
                ViewBag.HasHistory = false;
                return View();
            }
            else
            {
                ViewBag.HasHistory = true;
                var labelList = surveyList.Select(e => e.SegSurveyDate).ToList();
                var dataList = surveyList.Select(e => e.CopacesRating).ToList();
                var chartData = new
                {
                    labels = labelList,
                    datasets = new[]
                    {
                        new
                        {
                            label = "Rating History",
                            fillColor = "rgba(220,220,220,0.2)",
                            strokeColor = "rgba(220,220,220,1)",
                            pointColor = "rgba(220,220,220,1)",
                            pointStrokeColor = "#fff",
                            pointHighlightFill = "#fff",
                            pointHighlightStroke = "rgba(220,220,220,1)",
                            data = dataList
                        }
                    }
                }; 
                return View(chartData);
            }
        }

        //public async Task<ActionResult> Search(SegmentSearchViewModel searchViewModel)
        public async Task<ActionResult> Search(SegmentSearchViewModel searchViewModel)
        {
            if (User.Identity.IsAuthenticated == false)
                return View();

            if (!searchViewModel.IsEmptyQuery)
                searchViewModel.Result = await ApiHelper.SearchSegments(
                    from: searchViewModel.From,
                    to: searchViewModel.To,
                    roadId: searchViewModel.RoadId,
                    placeId: CurrentUser.PlaceId);

            var roads = await ApiHelper.GetRoadsByPlaceId(CurrentUser.PlaceId);
            roads.Insert(0, new Road { Id = null, Name = string.Empty });
            ViewBag.RoadList = new SelectList(roads, "Id", "Name", String.Empty);

            return View(searchViewModel);
        }
    }
}
    