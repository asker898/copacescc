﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Copaces.CityCounty.Website.Models;
using Copaces.CityCounty.Website.Models.DataObjects;
using Copaces.CityCounty.Website.Utility;
using System.Threading.Tasks;


namespace Copaces.CityCounty.Website.Controllers
{
    public class HomeController : Controller
    {
        


        public ActionResult Index()
        {
            string curUserId = User.Identity.GetUserId();
            if (curUserId != null)
            {
                TempData["UserID"] = curUserId;
                return RedirectToAction("DashBoard", "DataManagementView");
                //return RedirectToAction("Index", "DataManagementView", new { id = curUserId });

            }
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}