﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Linq;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Web;
using System.Web.Mvc;
using Copaces.CityCounty.Website.Models;
using Copaces.CityCounty.Website.Models.DataObjects;
using Copaces.CityCounty.Website.Utility;
using System.Threading.Tasks;

namespace Copaces.CityCounty.Website.Controllers
{
    public class RoadSegmentsController : Controller
    {

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }



        // GET: RoadSegments
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult RoadSegments(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var road = ApiHelper.GetRoadById(id).Result;
            if (road == null)
            {
                return HttpNotFound();
            }
            var segments = ApiHelper.GetSegmentsByRoad(road).Result;
            var data = new RoadSegmentsViewModel(segments, road);
            return View(data);
        }



    }
}