﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Linq;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Web;
using System.Web.Mvc;
using Copaces.CityCounty.Website.Models;
using Copaces.CityCounty.Website.Models.DataObjects;
using Copaces.CityCounty.Website.Utility;
using System.Threading.Tasks;
using Microsoft.Owin.Security;

namespace Copaces.CityCounty.Website.Controllers
{
    public class DataManagementViewController : Controller
    {


        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }



        // GET: DataManagementView
        public async Task<ActionResult> DashBoard()
        {
            var curUser = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            var userPlaceId = curUser.PlaceId;
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());

            var segmentSurveys = await ApiHelper.GetSegmentSurveysByPlaceID(userPlaceId);
            var limitedSurveys = segmentSurveys.OrderByDescending(x => x.SegSurveyDate).Take(15);
            var dataview = new DataManagementViewModel(userPlaceId, segmentSurveys);

            return View(dataview);
        }





    }
}