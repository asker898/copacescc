﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Linq;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Web;
using System.Web.Mvc;
using Copaces.CityCounty.Website.Models;
using Copaces.CityCounty.Website.Models.DataObjects;
using Copaces.CityCounty.Website.Utility;
using System.Threading.Tasks;
using Microsoft.Owin.Security;
using OfficeOpenXml;
using OfficeOpenXml.Drawing;
using EPPlusEnumerable;
using OfficeOpenXml.Style;



namespace Copaces.CityCounty.Website.Controllers
{
    public class ReportController : Controller
    {

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public async Task<ActionResult> Index()
        {
            var curUser = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            var userPlaceID = curUser.PlaceId;

            List<SegmentSurvey> surveys = await ApiHelper.GetSegmentSurveysByPlaceID(userPlaceID);
            DashBoardViewModel data = await DataTools.Get_X_Year_Road_Information(userPlaceID);

            ReportViewModel result = new ReportViewModel(surveys, data, curUser.PlaceName, ApiHelper.GetPlaceById(curUser.PlaceId).Result.Type);

            return View(result);
        }


        public void GenerateSummray()
        {
            var user = UserManager.FindByIdAsync(User.Identity.GetUserId()).Result;
            List<SegmentSurvey> surveys = ApiHelper.GetSegmentSurveysByPlaceID(user.PlaceId).Result;
            DashBoardViewModel X_Year_data = DataTools.Get_X_Year_Road_Information(user.PlaceId).Result;

            ReportViewModel result = new ReportViewModel(surveys, X_Year_data, user.PlaceName, ApiHelper.GetPlaceById(user.PlaceId).Result.Type);
            using (var excelPackage = new ExcelPackage())
            {
                // edit overall information for summary report
                excelPackage.Workbook.Properties.Author = "COPACES-CC Data Management System";
                excelPackage.Workbook.Properties.Company = "Georgia Institute of Technology";
                excelPackage.Workbook.Worksheets.Add("Summary Report");
                excelPackage.Workbook.Worksheets.Add("Detailed Report");
                var sheet1 = excelPackage.Workbook.Worksheets[1];
                sheet1.Name = "Summary Report";
                sheet1.Cells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                sheet1.Cells.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                sheet1.Cells.Style.Font.Size = 9;
                sheet1.Cells.Style.Font.Name = "Calibri";

                var sheet2 = excelPackage.Workbook.Worksheets[2];
                sheet2.Cells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                sheet2.Cells.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                sheet2.Cells.Style.Font.Size = 9;
                sheet2.Cells.Style.Font.Name = "Calibri";


                
                // header, 2 rows
                sheet1.Cells["A1:I1"].Merge = true;
                sheet1.Cells["A1"].Value = "Segment Condition Summary for " + result.Jurisdiction + " " + result.PlaceType.ToUpper();
                sheet1.Cells["A1"].Style.Font.Size = 13;
                sheet1.Cells["A1"].Style.Font.Bold = true;
                sheet1.Cells["A2:C2"].Merge = true;
                sheet1.Cells[2, 1].Value = "Total Road Mileage: " + result.ReportData.TotalLength;
                sheet1.Cells["G2:I2"].Merge = true;
                sheet1.Cells["G2"].Value = "Report generated on " + result.dateTime;

                sheet2.Cells["A1:AA1"].Merge = true;
                sheet2.Cells["A1"].Value = "Segment Condition Summary for " + result.Jurisdiction + " " + result.PlaceType.ToUpper();
                sheet2.Cells["A1"].Style.Font.Size = 13;
                sheet2.Cells["A1"].Style.Font.Bold = true;
                sheet2.Cells["A2:C2"].Merge = true;
                sheet2.Cells[2, 1].Value = "Total Road Mileage: " + result.ReportData.TotalLength;
                sheet2.Cells["X2:AA2"].Merge = true;
                sheet2.Cells["X2"].Value = "Report generated on " + result.dateTime;

                sheet1.Cells["A3"].Value = "District";
                sheet1.Cells["B3"].Value = "Road ID";
                sheet1.Cells["C3"].Value = "Road Name";
                sheet1.Cells["D3"].Value = "Inventory ID";
                sheet1.Cells["E3"].Value = "From";
                sheet1.Cells["F3"].Value = "To";
                sheet1.Cells["G3"].Value = "Survey Date";
                sheet1.Cells["H3"].Value = "COPACES Rating";
                sheet1.Cells["I3"].Value = "Windshield Score";



                // lack sheet2.Cells["A3"]

                sheet2.Cells["A4:C4"].Merge = true;
                sheet2.Cells["A4"].Value = "Location";
                sheet2.Cells["D4:E4"].Merge = true;
                sheet2.Cells["D4"].Value = "Rut Depth";
                sheet2.Cells["F4:G4"].Merge = true;
                sheet2.Cells["F4"].Value = "Load Cracking";
                sheet2.Cells["H4:I4"].Merge = true;
                sheet2.Cells["H4"].Value = "Block Cracking";
                sheet2.Cells["E4"].Value = "Refection Cracking";
                sheet2.Cells["F4"].Value = "Raveling";
                sheet2.Cells["G4"].Value = "Edge Distress";
                sheet2.Cells["H4"].Value = "Bleeding/ Flushing";
                sheet2.Cells["I4"].Value = "Corrugation/ Pushing";
                sheet2.Cells["J4"].Value = "Loss Pavement Section";
                sheet2.Cells["L4"].Value = "COPACES Rating";
                sheet2.Cells["H4"].Value = "Windshild Survey";






                int row=4; 
                foreach (var item in surveys)
                {
                    int col = 1;
                    var road = ApiHelper.GetRoadById(item.RoadId).Result;
                    var seg = ApiHelper.GetSegmentByRoadIdAndSequenceId(item.RoadId, item.SequenceId).Result;
                    sheet1.Cells[row, col++].Value = road.District;
                    sheet1.Cells[row, col++].Value = item.RoadId;
                    sheet1.Cells[row, col++].Value = road.Name;
                    sheet1.Cells[row, col++].Value = item.InventId;
                    sheet1.Cells[row, col++].Value = seg.From;
                    sheet1.Cells[row, col++].Value = seg.To;
                    var datatime = (DateTime)item.RoadSurveyDate;
                    sheet1.Cells[row, col++].Value = datatime.ToString("d");
                    if (item.CopacesRating != null)
                    {
                        sheet1.Cells[row, col++].Value = item.CopacesRating;
                    }
                    else
                    {
                        sheet1.Cells[row, col++].Value = "";
                    }
                    if (item.IsWindshieldSurvey)
                    {
                        sheet1.Cells[row, col++].Value = item.WindshieldScore;
                    }
                    if (row % 4 == 0)
                    {
                        for (int i = 1; i < 10; i++)
                        {
                            sheet1.Cells[row, i].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        }
                    }

                    for (int i = 1; i < 10; i++)
                    {
                        sheet1.Cells[row,i].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                    }

                    row++;
                }
                for (int i = 1; i < 10; i++)
                {
                    sheet1.Cells[row, i].Style.Border.Top.Style = ExcelBorderStyle.Thin;

                }



                // set columns style
                sheet1.Column(1).Width = 12;
                sheet1.Column(2).Width = 13.5;
                sheet1.Column(3).Width = 13;
                sheet1.Column(4).Width = 9.4;
                sheet1.Column(5).Width = 15.5;
                sheet1.Column(6).Width = 15.5;
                sheet1.Column(7).Width = 15;
                sheet1.Column(8).Width = 16;
                sheet1.Column(9).Width = 16;
                for (int i = 1; i < 10; i++)
                {
                    sheet1.Cells[3, i].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    sheet1.Cells[3, i].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                }

                    // save to memory stream;
                 Response.ClearContent();
                Response.BinaryWrite(excelPackage.GetAsByteArray());
                Response.AddHeader("content-disposition",
                          "attachment;filename=results.xlsx");
                Response.ContentType = "application/excel";
                Response.Flush();
                Response.End();




            }




        }




        public void GenerateDetails()
        {
            var user = UserManager.FindByIdAsync(User.Identity.GetUserId()).Result;
           List<SegmentSurvey> surveys = ApiHelper.GetSegmentSurveysByPlaceID(user.PlaceId).Result;
            DashBoardViewModel X_Year_data = DataTools.Get_X_Year_Road_Information(user.PlaceId).Result;

            ReportViewModel result = new ReportViewModel(surveys, X_Year_data, user.PlaceName, ApiHelper.GetPlaceById(user.PlaceId).Result.Type);
            using (var excelPackage = new ExcelPackage())
            {
                excelPackage.Workbook.Properties.Author = "COPACES-CC Data Management System";
                excelPackage.Workbook.Properties.Company = "Georgia Institute of Technology";
                excelPackage.Workbook.Worksheets.Add("Survey Condition Report");
                var sheet1 = excelPackage.Workbook.Worksheets[1];
                sheet1.Name = "Summary Report";
                sheet1.Cells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                sheet1.Cells.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                sheet1.Cells.Style.Font.Size = 9;
                sheet1.Cells.Style.Font.Name = "Calibri";

                // first 3 rows
                sheet1.Cells["A1:I1"].Merge = true;
                sheet1.Cells["A1"].Value = "Segment Condition Summary for " + result.Jurisdiction + " " + result.PlaceType.ToUpper();
                sheet1.Cells["A1"].Style.Font.Size = 13;
                sheet1.Cells["A1"].Style.Font.Bold = true;

                sheet1.Cells["A2:C2"].Merge = true;
                sheet1.Cells[2, 1].Value = "Total Road Mileage: " + result.ReportData.TotalLength;
                sheet1.Cells["G2:I2"].Merge = true;
                sheet1.Cells["G2"].Value = "Report generated on " + result.dateTime;

                sheet1.Cells["A3"].Value = "District";
                sheet1.Cells["B3"].Value = "Road ID";
                sheet1.Cells["C3"].Value = "Road Name";
                sheet1.Cells["D3"].Value = "Inventory ID";
                sheet1.Cells["E3"].Value = "From";
                sheet1.Cells["F3"].Value = "To";
                sheet1.Cells["G3"].Value = "Survey Date";
                sheet1.Cells["H3"].Value = "COPACES Rating";
                sheet1.Cells["I3"].Value = "Windshield Score";






                // save to memory stream;
                Response.ClearContent();
                Response.BinaryWrite(excelPackage.GetAsByteArray());
                Response.AddHeader("content-disposition",
                          "attachment;filename=results.xlsx");
                Response.ContentType = "application/excel";
                Response.Flush();
                Response.End();
            }



        }
    }
}
