﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Copaces.CityCounty.Website.Models.DataObjects;
using Newtonsoft.Json;
using System.Runtime.Serialization.Json;
using System.IO;
using MiscUtil;
using Copaces.CityCounty.Website.Models;

namespace Copaces.CityCounty.Website.Utility
{
    public static class DataTools
    {
        private static string TransformMonthToString(int month)
        {
            switch (month)
            {
                case 1: return "Jan.";
                case 2: return "Feb.";
                case 3: return "Mar.";
                case 4: return "Apr.";
                case 5: return "May.";
                case 6: return "Jun.";
                case 7: return "Jul.";
                case 8: return "Aug.";
                case 9: return "Sep.";
                case 10: return "Oct.";
                case 11: return "Nov.";
                case 12: return "Dec.";
            }
            return null;
        }
        public static string[] GetDateabels()
        {
            string[] labels = new string[6];
            for (int i = 0; i < 6; i++)
            {
                string year = DateTime.Today.AddMonths(-i).Year.ToString();
                string month = TransformMonthToString(DateTime.Today.AddMonths(-i).Month);
                    
                labels[i] = month + " " + year;
            }
            return labels;
        }


        public static async Task<int[]> GetAverageScore(string placeID)
        {
            var allSegments = await ApiHelper.GetSegmentSurveysByPlaceID(placeID);
            var surveyed = allSegments.Where(x => x.SegSurveyDate != null);
            // For Line Chart, Calculate avg Copacescc rating score.
            int[] avgScore = new int[6]; int[] surveyCount = { 0, 0, 0, 0, 0, 0 }; int[] totalScore = { 0, 0, 0, 0, 0, 0 };
            foreach (var x in surveyed)
            {
                if (Convert.ToDateTime(x.SegSurveyDate).CompareTo(DateTime.Now.AddMonths(-2)) >= 0)
                {
                    totalScore[0] += Convert.ToInt32(x.CopacesRating);
                    surveyCount[0] += 1;
                }
                for (int i = 1; i < 6; i++)
                {
                    if (Convert.ToDateTime(x.SegSurveyDate).CompareTo(DateTime.Now.AddMonths(-2 * (i + 1))) >= 0 && Convert.ToDateTime(x.SegSurveyDate).CompareTo(DateTime.Now.AddMonths(-2 * i)) < 0)
                    {
                        totalScore[i] += Convert.ToInt32(x.CopacesRating);
                        surveyCount[i] += 1;
                        break;
                    }

                }
            }
            for (int i = 0; i < 6; i++)
            {
                if (surveyCount[i] == 0)
                {
                    avgScore[i] = 0;
                }
                else
                {
                    avgScore[i] = totalScore[i] / surveyCount[i];
                }
            }
            return avgScore;
        }



        // for Dashboard, calculate unfinished/ finished/ uninitialized roads
        public static async Task<DashBoardViewModel> Get_X_Year_Road_Information(string placeID)
        {
            List<Segment> segments = await ApiHelper.GetSegmentsByPlace(new Place { Id = placeID });
            List<Road> roads = await ApiHelper.GetRoadsByPlaceId(placeID);
            List<SegmentSurvey> surveys = await ApiHelper.GetSegmentSurveysByPlaceID(placeID);


            // Step 1. Calculate total road length

            double? totalLength = 0;
            double? inactiveLength = 0;


            // for all of the years
            foreach (var seg in segments)
            {
                totalLength += seg.Length;
            }

            var roads_inactivated = roads.Where(x => x.LastSurveyDate == null);
            var roads_unfinished = roads.Where(x => x.HasUnfinishedSurvey == false && x.LastSurveyDate == null);
            int UnfinishedLength = 0;
            HashSet<string> roadList = new HashSet<string>();
            foreach (var ria in roads_inactivated)
            {
                roadList.Add(ria.Id);
                foreach (var seg in ApiHelper.GetSegmentsByRoad(ria).Result)
                {
                    inactiveLength += seg.Length;
                }
            }

            var uninitiatedLength = Convert.ToInt32(inactiveLength);
            //var FinishedLength = Convert.ToInt32(totalLength) - UninitiatedLength - UnfinishedLength;
            //

            HashSet<int> segmentList = new HashSet<int>();
            segmentList.Clear();
            var surveyed = surveys.Where(x => x.SegSurveyDate != null);
            int FinishedLength = 0;
            foreach (var segSurvey in surveyed)
            {
                var seg = ApiHelper.GetSegmentByRoadIdAndSequenceId(segSurvey.RoadId, segSurvey.SequenceId).Result;
                if (!segmentList.Contains(seg.InventId))
                {
                    FinishedLength += Convert.ToInt32(seg.Length);
                    segmentList.Add(seg.InventId);
                }
            }
            UnfinishedLength = Convert.ToInt32(totalLength) - FinishedLength - uninitiatedLength;

            var pctcompletion = FinishedLength * 100 / Convert.ToInt32(totalLength);


            // To calculate the nearest 3 years.
            int[] unfinished_X_yearLength = new int[3];
            int[] finished_X_yearLength = new int[3];
            for (int i = 0; i < 3; i++)
            {
                finished_X_yearLength[i] = 0;
                unfinished_X_yearLength[i] = 0;
                var Date_X_years = DateTime.Today.AddYears(-i - 1);
                var surveys_within_X_Years = surveys.Where(x => Convert.ToDateTime(x.SegSurveyDate).CompareTo(Date_X_years) >= 0);
                segmentList.Clear();
                foreach (var segSurvey in surveys_within_X_Years)
                {
                    var seg = ApiHelper.GetSegmentByRoadIdAndSequenceId(segSurvey.RoadId, segSurvey.SequenceId).Result;
                    if (!segmentList.Contains(seg.InventId))
                    {
                        finished_X_yearLength[i] += Convert.ToInt32(seg.Length);
                        segmentList.Add(seg.InventId);
                    }
                    unfinished_X_yearLength[i] = Convert.ToInt32(totalLength) - finished_X_yearLength[i] - uninitiatedLength;
                }
            }

            // End of Step 3
            DashBoardViewModel results = new DashBoardViewModel();
            results.TotalLength = Convert.ToInt32(totalLength);
            results.Unfinished_X_yearLength = unfinished_X_yearLength;
            results.pct = pctcompletion;
            results.PlaceID = placeID;
            results.Segments_Count = segments.Count;
            results.UninitiatedLength = uninitiatedLength;
            results.Finished_X_yearLength = finished_X_yearLength;
            results.surveysCount = surveys.Count;


            return results;
        }

        public static async Task<int> GetRoadCount(string placeId)
        {
            List<Road> roads = await ApiHelper.GetRoadsByPlaceId(placeId);
            return roads.Count;
        }

        public static async Task<int[]> GetScoreDistribution(string placeID)
        {
            var surveys = await ApiHelper.GetSegmentSurveysByPlaceID(placeID);
            int[] distribution = { 0, 0, 0, 0, 0, 0 };
            foreach (var segvey in surveys)
            {
                if (segvey.CopacesRating > 90)
                {
                    distribution[0]++;
                    continue;
                }
                if (segvey.CopacesRating >= 80 && segvey.CopacesRating < 90)
                {
                    distribution[1]++;
                    continue;
                }
                if (segvey.CopacesRating >= 70 && segvey.CopacesRating < 80)
                {
                    distribution[2]++;
                    continue;
                }
                if (segvey.CopacesRating >= 60 && segvey.CopacesRating < 70)
                {
                    distribution[3]++;
                    continue;
                }
                if (segvey.CopacesRating >= 50 && segvey.CopacesRating < 60)
                {
                    distribution[4]++;
                    continue;
                }
                if (segvey.CopacesRating < 50)
                {
                    distribution[5]++;
                    continue;
                }
            }
            return distribution;
        }






    }
}