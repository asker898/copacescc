﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Copaces.CityCounty.Website.Models.DataObjects;
using Newtonsoft.Json;
using System.Runtime.Serialization.Json;
using System.IO;
using MiscUtil;


namespace Copaces.CityCounty.Website.Utility
{
    public static class ApiHelper
    {
        private static string BaseURL = "https://copacesccmob.azure-mobile.net/";
        private static string mobileServiceKey = "AErAzrgWyHfccdGxqoSKTqIUNAhoLO26";
        public static HttpClient Client { get; set; }

        static ApiHelper()
        {
            Client = new HttpClient();
            Client.DefaultRequestHeaders.Accept.Add(
                new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            Client.DefaultRequestHeaders.Add("x-zumo-application",mobileServiceKey);
        }

        static async Task<string> GetAsyncWithCred(this HttpClient client, string apiUrl)
        {
            string responseBody;

            using (var response = client.GetAsync(BaseURL + apiUrl).Result)
            {
                response.EnsureSuccessStatusCode();
                responseBody = await response.Content.ReadAsStringAsync();
            }
            return responseBody;
        }

        public static async Task<List<Place>> GetPlaces()
        {
            var response = await Client.GetAsyncWithCred("tables/place");
            var list = JsonConvert.DeserializeObject<List<Place>>(response);
            return list;
        }


        public static async Task<Place> GetPlaceById(string id)
        {
            if (String.IsNullOrEmpty(id)) return null;
            var url = String.Format("tables/place/{0}", id);
            return JsonConvert.DeserializeObject<Place>(await Client.GetAsyncWithCred(url));
        }

        public static async Task<List<Road>> GetRoadsByPlace(Place place)
        {
            if (place.Id == null) return null;
            var url = String.Format("tables/road?$filter=(placeId+eq+'{0}')", place.Id);
            return JsonConvert.DeserializeObject<List<Road>>(await Client.GetAsyncWithCred(url));
        }


        public static string GetNextRoadId()
        {
            string id = DateTime.Now.ToString("yyMMddhhmmss") + StaticRandom.Next(1, 100).ToString();
            return id;

        }

        public static async Task<int> GetNextSegmentInventId(string placeId)
        {
            //var url = String.Format("tables/segment?$filter=placeId eq '{0}'", placeId);
            //var segments=  JsonConvert.DeserializeObject<List<Segment>>(await Client.GetAsyncWithCred(url))
            //    .ToList();
            //int largestInventId = 0;
            //foreach (Segment seg in segments)
            //{
            //    if (seg.InventId>largestInventId){
            //        largestInventId = seg.InventId;
            //    }
            //}
            //return largestInventId;
            return Convert.ToInt32(DateTime.Now.ToString("yyMMddhhmmss") + StaticRandom.Next(1, 100).ToString());
        }

        public static async Task DeleteRoadByID(string id)
        {
            try
            {
                var url = BaseURL + String.Format("tables/road/{0}", id);
                var aResponse = await Client.DeleteAsync(url);
            }
            catch (Exception ex)
            {
                var errorMessage = ex.ToString();
            }
        }


        public static async Task EditRoad(Road road)
        {
            try
            {
                string theUri = BaseURL + "tables/road";
                //Create a Json Serializer for our type 
                String jsonSer = JsonConvert.SerializeObject(road);
                HttpContent payload = new StringContent(jsonSer, System.Text.Encoding.UTF8, "application/json");

                //Post the data 
                await DeleteRoadByID(road.Id);
                var res = await Client.PostAsync(theUri, payload);
            }
            catch (System.Net.WebException ex)
            {
                Console.WriteLine(ex.ToString());
                Console.ReadLine();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.ToString());
                Console.Read();
            }
        }

        public static async Task CreateRoad(Road road)
        {
            try
            {
                string theUri = BaseURL+"tables/road";
                //Create a Json Serializer for our type 
                String jsonSer = JsonConvert.SerializeObject(road);
                HttpContent payload = new StringContent(jsonSer, System.Text.Encoding.UTF8, "application/json");

                //Post the data 
                var aResponse = await Client.PostAsync(theUri, payload);
            }
            catch (System.Net.WebException ex)
            {
                Console.WriteLine(ex.ToString());
                Console.ReadLine();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.ToString());
                Console.Read();
            }

        }

        public static async Task CreateSegmentSurvey(SegmentSurvey  survey)
        {
            try
            {
                string theUri = BaseURL + "tables/segmentsurvey";
                //Create a Json Serializer for our type 
                String jsonSer = JsonConvert.SerializeObject(survey);
                HttpContent payload = new StringContent(jsonSer, System.Text.Encoding.UTF8, "application/json");

                //Post the data 
                var aResponse = await Client.PostAsync(theUri, payload);
            }
            catch (System.Net.WebException ex)
            {
                Console.WriteLine(ex.ToString());
                Console.ReadLine();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.ToString());
                Console.Read();
            }

        }

        public static async Task EditSegmentSurvey(SegmentSurvey survey)
        {
            try
            {
                string theUri = BaseURL + "tables/segmentsurvey";
                //Create a Json Serializer for our type 
                String jsonSer = JsonConvert.SerializeObject(survey);
                HttpContent payload = new StringContent(jsonSer, System.Text.Encoding.UTF8, "application/json");

                //Post the data 
                await DeleteSegmentSurveyByID(survey.Id);
                var aResponse = await Client.PostAsync(theUri, payload);
            }
            catch (System.Net.WebException ex)
            {
                Console.WriteLine(ex.ToString());
                Console.ReadLine();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.ToString());
                Console.Read();
            }

        }


        


        public static async Task DeleteSegmentSurveyByID(string id)
        {
            try
            {
                var url = BaseURL + String.Format("tables/segmentsurvey/{0}", id);
                var aResponse = await Client.DeleteAsync(url);
            }
            catch (Exception ex)
            {
                var errorMessage = ex.ToString();
            }
        }


        public static async Task EditSegment(Segment  segment)
        {
            try
            {
                string theUri = BaseURL + "tables/segment";
                //Create a Json Serializer for our type 
                String jsonSer = JsonConvert.SerializeObject(segment);
                HttpContent payload = new StringContent(jsonSer, System.Text.Encoding.UTF8, "application/json");

                //Post the data 
                await DeleteSegmentByID(segment.Id);
                var aResponse = await Client.PostAsync(theUri, payload);
            }
            catch (System.Net.WebException ex)
            {
                Console.WriteLine(ex.ToString());
                Console.ReadLine();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.ToString());
                Console.Read();
            }
        }

        public static async Task DeleteSegmentByID(string id)
        {
            try
            {
                var url = BaseURL + String.Format("tables/segment/{0}", id);
                var aResponse = await Client.DeleteAsync(url);
            }
            catch (Exception ex)
            {
                var errorMessage = ex.ToString();
            }
        }

        public static async Task CreateSegment(Segment segment)
        {
            try
            {

                string theUri = BaseURL+ "/tables/segment";
                //Create a Json Serializer for our type 
                String jsonSer = JsonConvert.SerializeObject(segment);
                HttpContent payload = new StringContent(jsonSer, System.Text.Encoding.UTF8, "application/json");

                //Post the data 
                var aResponse = await Client.PostAsync(theUri, payload);
            }
            catch (System.Net.WebException ex)
            {
                Console.WriteLine(ex.ToString());
                Console.ReadLine();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.ToString());
                Console.Read();
            }

        }




        public static async Task<Road> GetRoadById(string id)
        {
            if (String.IsNullOrEmpty(id)) return null;
            var url = String.Format("tables/road/{0}", id);
            
            return JsonConvert.DeserializeObject<Road>(await Client.GetAsyncWithCred(url));
        }

        public static async Task<List<Road>> GetRoadsByPlaceId(string id)
        {
            return await GetRoadsByPlace(new Place {Id = id});
        }




        public static async Task<Segment> GetSegmentByRoadIdAndSequenceId(string RoadId, int? SequenceId)
        {
            var url = "tables/segment?$filter=(roadId+eq+'" + RoadId + "')";
            var temp2 = await Client.GetAsyncWithCred(url);
            var temp= JsonConvert.DeserializeObject<List<Segment>>(temp2);
            var res = temp.Where(x => x.SequenceId == SequenceId);
            return res.First();
  

        }




        public static async Task<List<RoadSurvey>> GetRoadSurveysByRoad(Road road)
        {
            if (road.Id == null) return null;
            var url = String.Format("tables/roadsurvey?$filter=roadId eq {0}", road.Id);
            return JsonConvert.DeserializeObject<List<RoadSurvey>>(await Client.GetAsyncWithCred(url));
        }

        public static async Task<List<Segment>> GetSegmentsByPlace(Place place)
        {
            if (place.Id == null) return null;
            var url = String.Format("tables/segment?$filter=placeId eq '{0}'", place.Id);
            return JsonConvert.DeserializeObject<List<Segment>>(await Client.GetAsyncWithCred(url))
                .OrderBy(s => s.InventId).ToList();
        }

        public static async Task<List<Segment>> SearchSegments(
            string from = null, 
            string to = null, 
            string roadId = null,
            string placeId = null)
        {
            var filters = new List<String>();
            if (from != null)
                filters.Add(String.Format("(substringof('{0}', from))", from));
            if (to != null)
                filters.Add(String.Format("(substringof('{0}', to))", to));
            if (placeId != null)
                filters.Add(String.Format("(placeId eq '{0}')", placeId));
            if (roadId != null)
                filters.Add(String.Format("(roadId eq '{0}')", roadId));

            var finalFilter = String.Join(" and ", filters);
            var url = String.IsNullOrEmpty(finalFilter) ? "tables/segment" : String.Format("tables/segment?$filter={0}", finalFilter);
            return JsonConvert.DeserializeObject<List<Segment>>(await Client.GetAsyncWithCred(url));
        }

        public static async Task<List<Segment>> GetSegmentsByRoad(Road road)
        {
            if (road.Id == null) return null;
            var url = String.Format("tables/segment?$filter=roadId eq '{0}'", road.Id);
            return JsonConvert.DeserializeObject<List<Segment>>(await Client.GetAsyncWithCred(url));
        }

        public static async Task<List<SegmentSurvey>> GetSegmentSurveysByRoadSurvey(RoadSurvey roadSurvey)
        {
            if (roadSurvey.Id == null) return null;
            var url = String.Format("tables/segmentSurvey?$filter=roadSurveyId eq '{0}'", roadSurvey.Id);
            return JsonConvert.DeserializeObject<List<SegmentSurvey>>(await Client.GetAsyncWithCred(url));
        }

        public static async Task<List<SegmentSurvey>> GetSegmentSurveysBySegment(Segment segment)
        {
            var url = String.Format("tables/segmentsurvey?$filter=inventId eq {0}", segment.InventId);
            var response = await Client.GetAsyncWithCred(url);
            return (JsonConvert.DeserializeObject<List<SegmentSurvey>>(response)).OrderBy(ss => ss.SegSurveyDate).ToList();
        }

        public static async Task<List<SegmentSurvey>> GetSegmentSurveysByPlace(Place place)
        {
            if (place.Id == null) return null;
            var url = String.Format("tables/segmentSurvey?$filter=placeId eq '{0}'", place.Id);
            return (JsonConvert.DeserializeObject<List<SegmentSurvey>>(await Client.GetAsyncWithCred(url)))
                .OrderByDescending(ss => ss.SegSurveyDate).ToList();
        }

        public static async Task<List<SegmentSurvey>> GetSegmentSurveysByPlaceID(string placeId)
        {
            var url = String.Format("tables/segmentSurvey?$filter=placeId eq '{0}'", placeId);
            return (JsonConvert.DeserializeObject<List<SegmentSurvey>>(await Client.GetAsyncWithCred(url)))
                .OrderByDescending(ss => ss.SegSurveyDate).ToList();
        }

        public static async Task<SegmentSurvey> GetSegmentSurveyById(String id)
        {
            var url = String.Format("tables/segmentSurvey/{0}", id);
            return JsonConvert.DeserializeObject<SegmentSurvey>(await Client.GetAsyncWithCred(url));
        }

        public static async Task<Segment> GetSegmentById(string id)
        {
            if (String.IsNullOrEmpty(id)) return null;
            var url = String.Format("tables/segment/{0}", id);
            return JsonConvert.DeserializeObject<Segment>(await Client.GetAsyncWithCred(url));
        }

        public static async Task<List<SegmentSurvey>> SearchSegmentSurveys(DateTime? start = null, DateTime? end = null, string placeId = null, string roadId = null, int? maxRating = null)
        {
            var filters = new List<String>();
            if (start.HasValue)
                filters.Add(String.Format("(segSurveyDate ge datetime'{0}')", start.Value.ToString("yyyy-MM-ddTHH:mm:ss")));
            if (end.HasValue)
                filters.Add(string.Format("(segSurveyDate le datetime'{0}')", end.Value.ToString("yyyy-MM-ddTHH:mm:ss")));
            if (placeId != null)
                filters.Add(String.Format("(placeId eq '{0}')", placeId));
            if (roadId != null)
                filters.Add(String.Format("(roadId eq '{0}')", roadId));
            if (maxRating != null)
                filters.Add(String.Format("(copacesRating le {0})", maxRating.Value));

            var finalFilter = String.Join(" and ", filters);
            var url = String.IsNullOrEmpty(finalFilter) ? "tables/segmentsurvey" : String.Format("tables/segmentsurvey?$filter={0}", finalFilter);
            return JsonConvert.DeserializeObject<List<SegmentSurvey>>(await Client.GetAsyncWithCred(url));
        }

        public static async Task<List<SurveyPhoto>>  GetSurveyPhotosBySurvey(SegmentSurvey segmentSurvey)
        {
            var url = String.Format("tables/surveyPhoto?$filter=segmentSurveyId eq '{0}'", segmentSurvey.Id);
            return JsonConvert.DeserializeObject<List<SurveyPhoto>>(await Client.GetAsyncWithCred(url));
        }
    }
}