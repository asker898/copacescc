﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Copaces.CityCounty.Website.Models.DataObjects
{
    public class Road
    {
        //[Required(ErrorMessage="Id is required right now. I will make the id automatically increase later. Just for testing purpose now")]
        public string Id { get; set; }

        [Range(0,1000,ErrorMessage="the range should be within 0 to 1000. Modify later")]
        public int? BridgeNum { get; set; }

        public double? BridgeWidth { get; set; }

        public int? CulvertAndPipeNum { get; set; }

        public String District { get; set; }

        public String From { get; set; }

        [Display(Name = "Functional Class")]
        public String FunctionalClass { get; set; }

        [Display(Name = "Has Unfinished Survey")]
        public bool HasUnfinishedSurvey { get; set; }

        //[Required(ErrorMessage="Jurisdiction is required right now. I will modify it later to avoid user type the place any more")]
        public String Jurisdiction { get; set; }

        public int? LaneNum { get; set; }

        [Display(Name = "Last Survey Date")]
        public DateTime? LastSurveyDate { get; set; }

        public String Name { get; set; }

        public String PavMarkingCondition { get; set; }

        public double? PavWidthMax { get; set; }

        public double? PavWidthMin { get; set; }

        public double? PavWidthTypical { get; set; }

        [ScaffoldColumn(false)]
        public String PlaceId { get; set; }

        //[Required(ErrorMessage="I will change the placetype based on user placeId later")]
        public String PlaceType { get; set; }

        public String Remarks { get; set; }

        public double? ShoulderWidthMax { get; set; }

        public double? ShoulderWidthMin { get; set; }

        public double? ShoulderWidthTypical { get; set; }

        [Display(Name = "Surface Type")]
        public String SurfaceType { get; set; }

        public String To { get; set; }

        public double? UnpavedShoulderWidth { get; set; }

        // Surveies
        public List<RoadSurvey> RoadSurveys { get; set; }

        public List<SegmentSurvey> SegmentSurveys { get; set; }

        public Place Place { get; set; }
    }
}