﻿using System;

namespace Copaces.CityCounty.Website.Models.DataObjects
{
    public class SurveyPhoto
    {
        public string Id { get; set; }

        public string SegmentSurveyId { get; set; }

        public string ResourceName { get; set; }

        public string ImageUri { get; set; }

        public double? Latitude { get; set; }

        public double? Longitude { get; set; }

        public DateTime? TimeTaken { get; set; }

        public SegmentSurvey SegmentSurvey { get; set; }
    }
}