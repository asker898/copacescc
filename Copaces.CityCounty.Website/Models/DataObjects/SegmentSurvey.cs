﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Copaces.CityCounty.Website.Models.DataObjects
{
    public class SegmentSurvey
    {
        [BasicInfo]
        public string Id { get; set; }

        [BasicInfo, Display(Name = "COPACES Rating")]
        public double? CopacesRating { get; set; }

        [DistressInfo, Display(Name = "Bleed Level")]
        public int? BleedLevel { get; set; }

        [DistressInfo, Display(Name = "Bleed Percentage")]
        public double? BleedPercentage { get; set; }

        [DistressInfo, Display(Name = "Block Level")]
        public int? BlockLevel { get; set; }

        [DistressInfo, Display(Name ="Block Percentage")]
        public double? BlockPercentage { get; set; }

        [DistressInfo, Display(Name = "Corrug Level")]
        public int? CorrugLevel { get; set; }

        [DistressInfo, Display(Name = "Corrug Percentage")]
        public double? CorrugPercentage { get; set; }

        [DistressInfo, Display(Name = "Cross Slope(L)")]
        public double? CrossSlopeLeft { get; set; }

        [DistressInfo, Display(Name = "Cross Slope(R)")]
        public double? CrossSlopeRight { get; set; }

        [DistressInfo, Display(Name = "Edge Level")]
        public int? EdgeLevel { get; set; }

        [DistressInfo, Display(Name = "Edge Percentage")]
        public double? EdgePercentage { get; set; }

        [BasicInfo, Display(Name = "Invent Id")]
        public int InventId { get; set; }

        [BasicInfo, Display(Name = "Is Windshield Survey")]
        public bool IsWindshieldSurvey { get; set; }

        [BasicInfo, Display(Name = "Lane Direction")]
        public String LaneDirection { get; set; }

        [BasicInfo, Display(Name = "Lane Number")]
        public String LaneNum { get; set; }

        [DistressInfo, Display(Name = "Load Level 1 (%)")]
        public double? LoadLevel1 { get; set; }

        [DistressInfo, Display(Name = "Load Level 2 (%)")]
        public double? LoadLevel2 { get; set; }

        [DistressInfo, Display(Name = "Load Level 3 (%)")]
        public double? LoadLevel3 { get; set; }

        [DistressInfo, Display(Name = "Load Level 4 (%)")]
        public double? LoadLevel4 { get; set; }

        [DistressInfo, Display(Name = "Loss of Pavement Level")]
        public int? LossPavLevel { get; set; }

        [DistressInfo, Display(Name = "Loss of Pavement (%)")]
        public double? LossPavPercentage { get; set; }

        [DistressInfo, Display(Name = "Patch/Pothole Number")]
        public int? PatchPotholeNum { get; set; }

        [BasicInfo, Display(Name = "Place Id")]
        public String PlaceId { get; set; }

        [BasicInfo]
        public String Rater { get; set; }

        [DistressInfo, Display(Name = "Ravel Level")]
        public int? RavelLevel { get; set; }

        [DistressInfo, Display(Name = "Ravel Percentage")]
        public double? RavelPercentage { get; set; }

        [DistressInfo, Display(Name = "Reflect Length")]
        public double? ReflectLength { get; set; }

        [DistressInfo, Display(Name = "Reflect Level")]
        public int? ReflectLevel { get; set; }

        [DistressInfo, Display(Name = "Reflect Number")]
        public double? ReflectNum { get; set; }

        [BasicInfo]
        public String Remarks { get; set; }

        [BasicInfo, Display(Name = "Road Id")]
        public String RoadId { get; set; }

        private DateTime? roadSurveyDate;

        [BasicInfo, Display(Name = "Road Survey Date")]
        public DateTime? RoadSurveyDate
        {
            get
            {
                var timeZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
                if (roadSurveyDate.HasValue)
                    return TimeZoneInfo.ConvertTimeFromUtc(roadSurveyDate.Value, timeZone);
                else
                    return null;
            }
            set { roadSurveyDate = value; }
        }

        [BasicInfo, Display(Name = "Road Survey Id")]
        public String RoadSurveyId { get; set; }

        [DistressInfo, Display(Name = "Rut In Wheelpath")]
        public double? RutInWp { get; set; }

        [DistressInfo, Display(Name = "Rut Out of Wheelpath")]
        public double? RutOutWp { get; set; }

        [BasicInfo, Display(Name = "Sample Location")]
        public String SampleLocation { get; set; }

        private DateTime? segSurveyDate;

        [BasicInfo, Display(Name = "Segment Survey Date")]
        public DateTime? SegSurveyDate
        {
            get
            {
                var timeZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
                if (segSurveyDate.HasValue)
                    return TimeZoneInfo.ConvertTimeFromUtc(segSurveyDate.Value, timeZone);
                return null;
            }
            set { segSurveyDate = value; }
        }

        [BasicInfo, Display(Name = "Seq Id")]
        public int? SequenceId { get; set; }

        [BasicInfo, Display(Name = "Treatment Method")]
        public String TreatmentMethod { get; set; }

        [BasicInfo, Display(Name = "Treatment Year")]
        public String TreatmentYear { get; set; }

        [BasicInfo, Display(Name = "Windshield Score")]
        public double? WindshieldScore { get; set; }

    }

}