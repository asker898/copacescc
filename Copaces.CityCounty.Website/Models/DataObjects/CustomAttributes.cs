﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Copaces.CityCounty.Website.Models.DataObjects
{
    /// <summary>
    /// Basic information
    /// </summary>
    public class BasicInfoAttribute : Attribute
    {
    }

    public class DistressInfoAttribute : Attribute
    {
    }
}