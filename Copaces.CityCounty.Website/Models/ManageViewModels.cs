﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Copaces.CityCounty.Website.Models.DataObjects;
using System;
namespace Copaces.CityCounty.Website.Models
{
    public class IndexViewModel
    {
        public bool HasPassword { get; set; }
        public IList<UserLoginInfo> Logins { get; set; }
        public string PhoneNumber { get; set; }
        public bool TwoFactor { get; set; }
        public bool BrowserRemembered { get; set; }
    }

    public class ManageLoginsViewModel
    {
        public IList<UserLoginInfo> CurrentLogins { get; set; }
        public IList<AuthenticationDescription> OtherLogins { get; set; }
    }

    public class FactorViewModel
    {
        public string Purpose { get; set; }
    }

    public class SetPasswordViewModel
    {
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class ChangePasswordViewModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class AddPhoneNumberViewModel
    {
        [Required]
        [Phone]
        [Display(Name = "Phone Number")]
        public string Number { get; set; }
    }

    public class VerifyPhoneNumberViewModel
    {
        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }

        [Required]
        [Phone]
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }
    }

    public class ConfigureTwoFactorViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
    }

    public class DataManagementViewModel
    {
        public List<SegmentSurvey> SegmentSurveys { get; set; }
        public string placeID { get; set; }

        public DataManagementViewModel(string placeID, List<SegmentSurvey> surs)
        {
            this.placeID = placeID;
            SegmentSurveys = surs;

        }
    }


    public class ReportViewModel
    {
        public List<SegmentSurvey> SegmentSurveys { get; set; }
        public DashBoardViewModel ReportData { get; set; }
        public string dateTime { get; set; }
        public string Jurisdiction { get; set; }
        public string PlaceType { get; set; }

        public ReportViewModel(List<SegmentSurvey> SegmentSurveys, DashBoardViewModel data, string placeName, string placeType)
        {
            //DateTime = DateTime.Now;
            dateTime = DateTime.Now.ToString();
            this.SegmentSurveys = SegmentSurveys;
            this.ReportData = data;
            Jurisdiction = placeName;
            PlaceType = placeType;
        }
    }


    public class RoadSegmentsViewModel
    {
        public List<Segment> Segments { get; set; }
        public Road road { get; set; }

        public RoadSegmentsViewModel()
        {
            Segments = null;
            road = null;

        }

        public RoadSegmentsViewModel(List<Segment> segs, Road road)
        {
            Segments = segs;
            this.road = road;
        }



    }

    public class DashBoardViewModel
    {
        public string PlaceID { get; set; }
        public int UninitiatedLength { get; set; }
        public int[] Unfinished_X_yearLength { get; set; }
        public int[] Finished_X_yearLength { get; set; }
        public int TotalLength { get; set; }
        public double? averageLength { get; set; }
        public double? inactiveLength { get; set; }
        public int pctCompletion { get; set; }
        public int Segments_Count { get; set; }
        public int surveysCount { get; set; }
        public int pct { get; set; }
    }















}