﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Copaces.CityCounty.Website.Models.DataObjects;
using Copaces.CityCounty.Website.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Copaces.CityCounty.Website.Test
{
    [TestClass]
    public class UnitTestApiHelper
    {
        [TestMethod]
        public async Task TestGetPlaces()
        {
            var placeList = await ApiHelper.GetPlaces();
            Assert.IsNotNull(placeList);
            Assert.IsTrue(placeList.Count > 0);
        }

        [TestMethod]
        public async Task TestGetRoadsByPlace()
        {
            var placeList = await ApiHelper.GetPlaces();
            var newtonCounty = placeList.First(p => p.Name == "NEWTON" && p.Type == "County");
            var roadList = await ApiHelper.GetRoadsByPlace(newtonCounty);
            Assert.IsNotNull(roadList);
            Assert.IsTrue(roadList.First().Jurisdiction == "NEWTON");
        }

        [TestMethod]
        public async Task TestGetSegmentsByPlace()
        {
            var placeList = await ApiHelper.GetPlaces();
            var newtonCounty = placeList.First(p => p.Name == "NEWTON" && p.Type == "County");
            var segList = await ApiHelper.GetSegmentsByPlace(newtonCounty);
            Assert.IsNotNull(segList);
            Assert.IsTrue(segList.First().PlaceId == newtonCounty.Id);
        }

        [TestMethod]
        public async Task TestGetPlaceById()
        {
            var placeList = await ApiHelper.GetPlaces();
            var newtonCounty = placeList.First(p => p.Name == "NEWTON" && p.Type == "County");
            var place = await ApiHelper.GetPlaceById(newtonCounty.Id);
            Assert.IsTrue(place.Name == "NEWTON");
        }

        [TestMethod]
        public async Task TestGetSegmentSurveysByRoad()
        {
            var placeList = await ApiHelper.GetPlaces();
            var newtonCounty = placeList.First(p => p.Name == "NEWTON" && p.Type == "County");
            var segSurveyList = await ApiHelper.GetSegmentSurveysByPlace(newtonCounty);
            Assert.IsNotNull(segSurveyList);
            Assert.IsTrue(segSurveyList.First().PlaceId == newtonCounty.Id);
        }

        [TestMethod]
        public async Task TestGetSegmentSurveysBetweenDates()
        {
            var end = DateTime.Now;
            var start = new DateTime(2012, 1, 1);
            var surveys = await ApiHelper.SearchSegmentSurveys(start, end);
            Assert.IsTrue(surveys.Count > 0);
        }

        [TestMethod]
        public async Task TestGetSegmentSurveysBySegment()
        {
            var segment = new Segment() {InventId = 77};
            var hist = await ApiHelper.GetSegmentSurveysBySegment(segment);
            Assert.IsNotNull(hist);
        }
    }
}
