﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.WindowsAzure.Mobile.Service;

namespace Copaces.CityCounty.Models
{
    public class SegmentSurvey : EntityData
    {
        public int? BleedLevel { get; set; }

        public double? BleedPercentage { get; set; }

        public int? BlockLevel { get; set; }

        public double? BlockPercentage { get; set; }

        public double? CopacesRating { get; set; }

        public int? CorrugLevel { get; set; }

        public double? CorrugPercentage { get; set; }

        public double? CrossSlopeLeft { get; set; }

        public double? CrossSlopeRight { get; set; }

        public int? EdgeLevel { get; set; }

        public double? EdgePercentage { get; set; }

        [ForeignKey("Segment")]
        public int InventId { get; set; }

        public bool IsWindshieldSurvey { get; set; }

        public String LaneDirection { get; set; }

        public String LaneNum { get; set; }

        public double? LoadLevel1 { get; set; }

        public double? LoadLevel2 { get; set; }

        public double? LoadLevel3 { get; set; }

        public double? LoadLevel4 { get; set; }

        public int? LossPavLevel { get; set; }

        public double? LossPavPercentage { get; set; }

        public int? PatchPotholeNum { get; set; }

        public Place Place { get; set; }

        [ForeignKey("Place")]
        public String PlaceId { get; set; }

        public String Rater { get; set; }

        public int? RavelLevel { get; set; }

        public double? RavelPercentage { get; set; }

        public double? ReflectLength { get; set; }

        public int? ReflectLevel { get; set; }

        public double? ReflectNum { get; set; }

        public String Remarks { get; set; }

        public Road Road { get; set; }

        [ForeignKey("Road")]
        public String RoadId { get; set; }

        public RoadSurvey RoadSurvey { get; set; }

        public DateTime? RoadSurveyDate { get; set; }

        [ForeignKey("RoadSurvey")]
        public String RoadSurveyId { get; set; }

        public double? RutInWp { get; set; }

        public double? RutOutWp { get; set; }

        public String SampleLocation { get; set; }

        public Segment Segment { get; set; }

        public DateTime? SegSurveyDate { get; set; }

        public int? SequenceId { get; set; }

        public String TreatmentMethod { get; set; }

        public String TreatmentYear { get; set; }

        public double? WindshieldScore { get; set; }

        public List<SurveyPhoto> SurveyPhotos { get; set; } 
    }
}