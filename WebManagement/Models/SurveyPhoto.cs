﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebManagement.Models
{
    public class SurveyPhoto
    {
        public string Id { get; set; }

        public string SegmentSurveyId { get; set; }

        public string resourceName { get; set; }

        public string imageUri { get; set; }

        public double? Latitude { get; set; }

        public double? Longitude { get; set; }

        public DateTime? TimeTaken { get; set; }

        public SegmentSurvey SegmentSurvey { get; set; }
    }
}